export interface AddressInfo {
  /**
   * Id value from Shippo or undefined
   */
  object_id?: string;
  /**
   * Field that Shippo returns, used to check if Shippo considers this a valid shipping address.
   */
  is_complete?: boolean;
  name?: string;
  /**
   * House number or building number of the address.
   * For example: 55 Weston Road. 55 is the number and Weston Road is the street1
   */
  number?: string;
  /**
   * Route returned by Google
   */
  street1?: string;
  /**
   * second line for apartment numbers
   */
  street2?: string;
  city?: string;
  state?: string;
  zip?: string;
  country?: string;
}

export interface AddressValidationResponse {
  address?: AddressInfo;
  is_valid: boolean;
  messages?: string[];
}
