import { ModelBase } from './base-record';
import {
  ReturnReason,
  ReturnMethod,
  TransactionTotalParts,
  ReturnCostType,
  Status,
} from './enums/returns';
import { Money } from './money';

export interface ReturnPolicy extends ModelBase {
  merchantID: string;
  /**
   * Policy name and identifier
   */
  name: string;
  /**
   * Policy description: customer facing
   */
  description: string;
  /**
   * Determines whether or not a customer can receive a refund
   * No refunds does not necessarily mean no returns
   */
  refundsAccepted: boolean;
  /**
   * Auto approve returns/refunds
   */
  autoApprove: boolean;
  /**
   * Extra costs being subtracted from refund amount if refunds are accepted
   * Merchants can add reasons such as return shipping label fee, restocking fee, etc
   */
  costsImposed?: ExtraCosts[];
  /**
   * How many days product is eligible for returns/refunds
   * Return may still be requested after the daysToReturn date. Subject to manual approval by merchant
   */
  daysToReturn: number;
  /**
   * Methods a customer can take to return an item
   * Will there be multiple options availabe? (i.e. ship or drop off?)
   */
  returnMethod: ReturnMethod;
  /**
   * Location where the customer will take their item to be returned
   * an array to support multple drop off locations for the customer to choose from
   * This does not necessarily have to be an address. This could also be the name of an accepted drop off site (i.e. The UPS Store)
   */
  dropOffAddress?: string[];
  /**
   * Part of the total transaction cost that is to be refunded to the customer if refunds are accepted
   * an array of each part of the total transaction cost to refund. Net product cost, boom fee, and sales tax must all be refunded
   * Additionally, the merchant can choose to also refund the shipping cost
   */
  transactionTotalPartsToRefund?: TransactionTotalParts[];
  /**
   * Return fees charged to the customer
   */
  returnCosts: ReturnCost[];
}

/**
 * Any other return fees the merchant may charge to the customer. This might include a restocking fee.
 */
export interface ExtraCosts {
  /**
   * Cost Identifier (i.e. restocking fee)
   */
  name: string;
  /**
   * Explanation provided to the customer regarding fee
   */
  description: string;
  /**
   * Amount charged to the customer
   */
  price: Money;
}

/**
 * The amount of money it costs to process the return. This would include any return shipping fees.
 */
export interface ReturnCost extends ExtraCosts {
  /**
   * The type of return cost (i.e. flat rate fee, shipping fee, etc)
   */
  type: ReturnCostType;
}

export interface ReturnRequest extends ModelBase {
  customerID: string;
  merchantID: string;
  /**
   * Status of refund
   * Refunds issued after return is completed?
   * Refund Status should be optional. Only applicable if refunds are accepted
   */
  refundStatus?: Status;
  /**
   * Status of return
   */
  returnStatus: Status;
  /**
   * Displays the return policy to the customer
   */
  merchantPolicyID: string;
  /**
   * Reason for the return/refund request. Customer may select multiple reasons
   */
  returnReason: ReturnReason[];
  /**
   * Customer can add aditional return reasons they feel are applicable
   */
  customReason?: string;
  /**
   * Method in which return will be handled (i.e. shipped, dropped off, no return)
   */
  returnMethod: ReturnMethod;
  /**
   * Original purchase transaction
   * should point to transaction id
   */
  purchaseTransactionID: string;
  /**
   * Only applicable if refunds are accepted
   */
  refundAmount?: Money;
  /**
   * New return transaction
   * should point to transaction id
   */
  returnTransactionID: string;
  /**
   * Comment section for customers who may have anything they would like to add
   */
  comment?: string;
}

export interface ReturnDispute extends ModelBase {
  /**
   * The return/refund request made by the customer
   */
  returnRequest: ReturnRequest;
  /**
   * Checking if the dispute is still open/pending
   */
  isOpen: boolean;
  /**
   * Comments recorded by Boom Admin regarding dispute?
   */
  comment: string;
}

/**
 * Interface to handle/save chat messages between customers and merchants
 * Chat history displayed on Return Request Status Page
 */
export interface Message {
  /**
   * The message from either the merchant or the customer
   */
  text: string;
  /**
   * merchant or customer
   */
  fromID: string;
  /**
   * merchant or customer
   */
  toID: string;
  /**
   * Should names also be displayed?
   */
}
