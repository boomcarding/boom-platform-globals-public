export enum MerchantTransactionStatus {
  PENDING = 'pending',
  COMPLETED = 'completed',
  CANCELLED = 'cancelled',
  FAILED = 'failed',
}
export enum MerchantTransactionType {
  RECURRING = 'recurring',
  ONE_TIME = 'one-time',
  RETURN = 'return',
}
