export enum BoomAccountStatus {
  ACTIVE = 'Active',
  CANCELLED = 'Cancelled',
}
