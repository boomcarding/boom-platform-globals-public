export enum ProductStatus {
  PENDING = 'pending',
  APPROVED = 'approved',
  DECLINED = 'declined',
}
