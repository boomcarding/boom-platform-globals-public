export enum TransactionType {
  FUNDING = 'funding',
  PURCHASE = 'purchase',
  CASHBACK = 'cashback', // added from API
  TRANSFER = 'transfer',
  RETURN = 'return',
  MERCHANT_WITHDRAWAL = 'merchant-withdrawal',
  CUSTOMER_BILLING = 'customer-billing', // added from API
}
export enum TransactionStatus {
  PENDING = 'pending',
  CANCELLED = 'cancelled',
  COMPLETED = 'completed',
  FAILED = 'failed',
  UNPROCESSED = 'unprocessed',
}
