/**
 * Mandatory for Fedex only. License type of the
 * recipient of the Alcohol Package.
 */
export enum FedExAlcoholRecipientType {
  CONSUMER = 'consumer',
  LICENSEE = 'licensee',
}
