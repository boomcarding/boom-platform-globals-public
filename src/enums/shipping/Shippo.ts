export enum RateAttributes {
  CHEAPEST = 'CHEAPEST',
  FASTEST = 'FASTEST',
  BESTVALUE = 'BESTVALUE',
  FREE_SHIPPING = 'FREE SHIPPING',
  SELF_SHIP = 'SELF SHIP',
}

export enum ShippoLabelType {
  PNG = 'PNG',
  PDF = 'PDF',
  PDF_4x6 = 'PDF_4x6',
  ZPLII = 'ZPLII',
}

export enum ShippoTransactionState {
  VALID = 'VALID',
  INVALID = 'INVALID',
}

export enum ShippoTransactionStatus {
  WAITING = 'WAITING',
  QUEUED = 'QUEUED',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  REFUNDED = 'REFUNDED',
  REFUND_PENDING = 'REFUNDPENDING',
  REFUND_REJECTED = 'REFUNDREJECTED',
}

export enum ShippoTrackingStatus {
  UNKNOWN = 'UNKNOWN',
  DELIVERED = 'DELIVERED',
  TRANSIT = 'TRANSIT',
  FAILURE = 'FAILURE',
  RETURNED = 'RETURNED',
}

export enum ShippoRefundStatus {
  QUEUED = 'QUEUED',
  PENDING = 'PENDING',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

/**
 * Providers that can provide insurance seperate from
 * Shippo's 3rd party Shipsurance insurance
 */
export enum InsuranceProvider {
  FEDEX = 'FEDEX',
  UPS = 'UPS',
  ONTRAC = 'ONTRAC',
}

/**
 * The state should only ever be undefined or 'VALID'
 */
export enum ShippoParcelResponseState {
  VALID = 'VALID',
}
