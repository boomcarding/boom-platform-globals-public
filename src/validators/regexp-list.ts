export const TaxCountriesRegex = /^(US)$/;

export const TaxRegionsRegex = /^(AK|AL|AR|AS|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY)$/;

//this one is used by loopback jsonToSchemaObject currently we don't support flags so we cannot use the i flag further research is needed to support it, there's a validator plugin which could be used
export const StatesUpperOrLowerRegex = /^(AK|AL|AR|AS|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY|ak|al|ar|as|az|ca|co|ct|dc|de|fl|ga|gu|hi|ia|id|il|in|ks|ky|la|ma|md|me|mi|mn|mo|ms|mt|nc|nd|ne|nh|nj|nm|nv|ny|oh|ok|or|pa|pr|ri|sc|sd|tn|tx|ut|va|vi|vt|wa|wi|wv|wy)$/;

export const AcceptedCurrencyRegex = /^USD$/;

export const AcceptedCurrencySymbolRegex = /^\$$/;

export const ShipmentMethodsRegex = /^(usps_priority|usps_priority_express|usps_first|usps_parcel_select|ups_standard|ups_ground|ups_saver|ups_3_day_select|ups_second_day_air|ups_second_day_air_am|ups_next_day_air|ups_next_day_air_saver|ups_next_day_air_early_am|ups_mail_innovations_domestic|ups_surepost|ups_surepost_bound_printed_matter|ups_surepost_lightweight|ups_surepost_media|ups_express|ups_express_1200|ups_express_plus|ups_expedited|self ship)$/;

export const ZipCodeRegex = /^(\d{5}(?:-\d{4})?)$/;

export const DistanceUnitRegex = /^(cm|ft|in|m|yd|mm)$/;

export const MassUnitRegex = /^(g|oz|lb|kg)$/;

export const CountryUpperOrLowerRegex = /^(US|us)$/;

export const ShippingLabelFileTypeRegex = /^(PNG|PDF|PDF_4x6|ZPLII)$/;

export const EmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const PriceRegex = /^\d+\.\d*$/;

//TODO: Create a global phone number verification function that covers this case
// https://boomcarding.atlassian.net/browse/BW-1146
// The purpose of this check is to make sure the number is in the +1########## format
// with no spaces or (, ), or -
export const PhoneRegex2 = /\+1\d{10}/; // Originally at api\src\controllers\transfers.controller.ts

export const PhoneRegex = /^[+]{0,1}[\s\./0-9]*$/; // Originally at api\src\constants\validation-patterns.ts
