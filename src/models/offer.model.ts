import { ModelBase } from '../base-record';
import { Money } from '../money';
import { Product } from './product.model';

export interface Offer extends ModelBase {
  cashBackPerVisit?: Money;
  conditions?: string[];
  description?: string;
  maxQuantity?: number;
  maxVisits?: number;
  merchantUID?: string;
  startDate?: number;
  title?: string;
  product: Product;
  expiration?: number;
  /**
   * object id of this offer's return policy. If this does not exist then the
   * policy of this offer's product is used instead
   */
  returnPolicy?: string;
}
