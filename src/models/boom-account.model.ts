import { ModelBase } from '../base-record';
import { BoomAccountStatus } from '../enums';
import { Money } from '../money';

export interface BoomAccount extends ModelBase {
  status?: BoomAccountStatus;
  balance?: Money;
  customerID?: string;
}
