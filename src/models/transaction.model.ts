import { ModelBase } from '../base-record';
import { Booking } from './booking.model';
import { TransactionStatus, TransactionType } from '../enums';
import { Money } from '../money';
import { Offer } from './offer.model';
import { Product } from './product.model';
import { TaxCode } from '../sales-tax';
import { Store } from './store.model';
import { BoomUser } from '../user';

import { AllOptionalExceptFor } from '../utils/type-modifiers';
export interface Transaction extends ModelBase {
  type?: TransactionType;
  amount?: Money;
  cashback?: Money;
  nonce?: string;
  sender?: AllOptionalExceptFor<BoomUser, 'uid'> | AllOptionalExceptFor<Store, '_id'>;
  /**
   * When a member sends funds to a friend that does not have an account yet, the receiver is set to something like { uid: null, contact: {phone: 123412345}}
   */
  receiver?: AllOptionalExceptFor<BoomUser, 'uid'> | AllOptionalExceptFor<Store, '_id'>;
  title?: string;
  status?: TransactionStatus;
  taxcode?: TaxCode;
  salestax?: Money;
  purchaseItem?: Partial<Offer> | Partial<Product>;
  boomcardSuffix?: string;
  booking?: Partial<Booking>;
  /**
   * This is the date (utc unix) that a transaction was resolved. Used to check return window
   */
  dateReceived?: number;
  /**
   * The money Boom collected as commission
   */
  commissionCollected?: Money;
  /**
   * The _id of a ShippingOrder
   */
  shippingOrderId?: string;
  /**
   * 8 char pseudo unique id
   */
  shortId: string;
}
