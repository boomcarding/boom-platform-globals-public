import { ModelBase } from '../base-record';
import { Category } from './category.model';
import { ProductStatus } from '../enums';
import { Money } from '../money';
import { PackageDetails } from '../shipping';
import { Store } from './store.model';

export interface Product extends ModelBase {
  //objectID?: string; // TODO: Review if this value can be removed, if updated review validations on API - Remove if all works ok
  imageUrl?: string;
  category?: Category;
  name?: string;
  description?: string;
  store?: Partial<Store>;
  price: Money;
  attributes?: object;
  _tags?: string[];
  merchantUID?: string;

  /**
   * Package details are needed for items that can be shipped
   */
  packageDetails?: PackageDetails;
  /**
   * this is an _id of a ShippingPolicy document
   */
  shippingPolicy: string;
  /**
   *  status shows if this item has been approved for sale on Boom
   */
  status: ProductStatus;
  /**
   * Provisional field to keep track of existing products
   */
  quantity: number;
  /**
   * object id of this product's return policy
   */
  returnPolicy: string;
}
