import { ModelBase } from '../base-record';
import { Money } from '../money';
import { BoomUser } from '../user';
import { StoreBasic } from './store.model';
import { InventoryItem } from './inventory-item.model';
import { InventoryOrderStatus, InventoryOrderBillingType, InventoryOrderType } from '../enums';
import { AllOptionalExceptFor } from '../utils/type-modifiers';

export interface InventoryOrder extends ModelBase {
  item?: AllOptionalExceptFor<InventoryItem, '_id' | 'itemID' | 'itemName'>;
  status?: InventoryOrderStatus;
  billingType?: InventoryOrderBillingType;
  orderType?: InventoryOrderType;
  amount?: Money;
  merchant?: AllOptionalExceptFor<BoomUser, 'uid'>;
  store?: AllOptionalExceptFor<StoreBasic, '_id'>;
  notes?: string;
}
