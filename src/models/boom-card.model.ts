import { ModelBase } from '../base-record';
import { BoomCardStatus } from '../enums';

export interface BoomCard extends ModelBase {
  cardNumber: string;
  pinNumber: number;
  status: BoomCardStatus;
  fromBatchId: string;
  qrcode: string;
  storeID?: string;
  customerID?: string;
  boomAccountID?: string;
  storeMerchantID?: string;
}
