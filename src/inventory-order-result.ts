import { InventoryOrder } from './models/inventory-order.model';
import { MerchantTransaction } from './merchant-transaction';

export interface InventoryOrderResult {
  success: boolean;
  message: string;
  inventoryOrders?: InventoryOrder[];
  merchantTransactions?: MerchantTransaction[];
}
