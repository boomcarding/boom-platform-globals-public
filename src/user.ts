import { ContactInfo } from './contact-info';
import { AddressInfo } from './address-info';
import { Money } from './money';
import { RoleKey } from './enums';
import { Store } from './models/store.model';
import { BoomUserPlaidInfo } from './bank-info';
import { Nexus } from './sales-tax';

export interface BoomUserBasic {
  uid?: string;
  firstName?: string;
  lastName?: string;
  contact?: ContactInfo;
  addresses?: AddressInfo[];
}
export interface ProfileImage {
  imgUrl?: string;
  imgFile?: File | null;
  base64Data?: string | null;
  previewImgUrl?: string | null;
  previewBase64Data?: string | null;
}

export interface BoomUser extends BoomUserBasic {
  createdAt?: number;
  updatedAt?: number;
  gender?: string;
  registrationStep?: number;
  finishedRegistration?: boolean;
  roles?: RoleKey[];
  cards?: string[] | undefined | null; // TODO: Review why we need undefined or null, should be changed to cards?: string[]; if updated review validations on API
  store?: Partial<Store>;
  profileImg?: ProfileImage;
  enableNotification?: boolean; // TODO: Notifications should have its own object
  notificationSound?: boolean;
  notificationVibration?: boolean;
  fcmToken?: string;
  range?: number;
  grossEarningsPendingWithdrawal?: Money;
  netEarningsPendingWithdrawal?: Money;
  tempPassword?: string;
  password?: string;
  plaidInfo?: BoomUserPlaidInfo[];
  hasCards?: boolean; // TODO: No needed we can review cards.length
  taxableNexus?: Nexus[];
  boomAccounts?: string[];
  forceUpdate?: boolean; // Used on our Firebase functions "functions\functions\src\user.ts"
}

export enum AccountInfoQueryTypes {
  BoomcardPin = 'boom-card-pin',
  UsernamePassword = 'username-password',
}
