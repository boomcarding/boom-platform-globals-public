// TODO: the file name should be changed to ModelBase and moved to models
export interface ModelBase {
  _id?: string;
  createdAt?: number;
  updatedAt?: number;
}
