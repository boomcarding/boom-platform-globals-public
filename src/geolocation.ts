export interface Geolocation {
  lat: number | null; //TODO: null must be now allowed here, if updated review validations on API
  lng: number | null;
}
