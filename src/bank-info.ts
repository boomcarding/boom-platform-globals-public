import { ModelBase } from ".";

/**
 * 9/15/20: We should try using UserBankInfo instead of this but this is still currently being used in several places.
 * Eventually this can be removed
 *
 * Top level user bank account info interface for an API result.
 * This is populated by querying Plaid using existing BoomUserPlaidInfo data retrieved from a Firebase profile
 */
export interface UserBankAccountInfo {
  relatedRecordId?: string;
  accountName: string;
  accountBalance: AccountBalanceInfo;
  accountNumber: PlaidAuthAccountNumbers;
  memberId?: string;
  memberFullName: string;
  memberAddress: string;
  memberPhoneNumber: string;
}

/**
 * The top level interface for a Boom User's plaid info, as saved in their Firebase profile. This info is partial and doesn't
 * contain full bank account info for security reasons. This data is used to query Plaid to retreive full account info.
 */
export interface BoomUserPlaidInfo {
  /**
   * Likely not being used
   */
  account: { id: any; mask: any; name: any; subtype: any; type: any };
  /**
   * Likely not being used
   */
  account_id: any | null;
  accounts: BoomUserPlaidAccount[];
  institution: { institution_id: string; name: string };
  item: { accessToken: string; itemId: string };
  link_session_id: string;
  public_token: string;
}

/**
 * Information about a single bank account, as stored in firebase
 * Example:
 * {
 *  id: 'yBBlWogGGwuZRMB3ZB5MUQGWl5r1N7Iy1lXRL',
 *  mask: '0000'
 *  name: 'Checking'
 *  subtype: 'checking'
 *  type: 'depository'
 * }
 *
 * Note: We don't store bank account numbers, only object references to Plaid. This is to follow best security practices.
 */
export interface BoomUserPlaidAccount {
  id: string;
  mask: string;
  name: string;
  subtype: string;
  type: string;
}

/**
 * 9/15/20: Instead of using this in the future we can put the numbers right into
 * a BankAccount object right next to the account balance
 *
 * Item holding actual bank account number and routing information in clear text
 */
export interface PlaidAuthAccountNumberInfo {
  account: string;
  account_id: string;
  routing: string;
  wire_routing: string;
}

/**
 * 9/15/20: This is used by bank-info.service but returning account numbers from the api
 *  in this format is not useful. There is a lot of api work that uses this so it should
 *  probably continue to be used within the api. For future work, we do not have to have
 * seperate lists for different types of account numbers, we can just add
 *
 * Collection of the different types of account number info items that can be retrieved
 */
export interface PlaidAuthAccountNumbers {
  achNumbers: PlaidAuthAccountNumberInfo[];
  eftNumbers: PlaidAuthAccountNumberInfo[];
  internationalNumbers: PlaidAuthAccountNumberInfo[];
  bacsNumbers: PlaidAuthAccountNumberInfo[];
}

/**
 *
 */
export interface AccountBalanceInfo {
  available: number;
  current: number;
  iso_currency_code: string;
  limit: number | null;
  unofficial_currency_code: string | null;
}

/**
 * The result of a Plaid service auth API request
 */
export interface PlaidAuthResult {
  numbers: PlaidAuthAccountNumbers;
  accounts: any;
}

/**
 * This is my new top level representation of a users bank info. It
 * is designed to be less confusing than UserBankAccountInfo, mainly
 * by grouping account balances with the related account number and
 * grouping accounts by institution.
 */
export interface UserBankInfo {
  userPhoneNumber: string;
  userFullName: string;
  userId?: string;
  institutions: BankInstitution[];
}

export interface BankInstitution {
  bankName: string;
  accounts: BankAccount[];
}

export interface BankAccount {
  accountholderName: string;
  accountholderAddress: string;
  accountType: string;
  accountNumber: string;
  routingNumber: string;
  wireRoutingNumber: string;
  accountID: string;
  balance: AccountBalanceInfo;
}

export interface AccountOwnerInfo {
  phone: string;
  names: string[];
  address: string;
  city: string;
  state: string;
  zip: string;
  emails: string[];
  gotInfoFromBank: boolean;
}
export interface PlaidAddress {
  data: {
    city: string;
    country: string;
    postal_code: string | null;
    region: string | null;
    street: string;
  };
  primary: boolean | null;
}

export interface PlaidAccountOwner {
  addresses: PlaidAddress[];
  emails: { data: string; primary: boolean; type: string }[];
  names: string[];
  phone_numbers: { data: string; primary: boolean | null; type: string | null }[];
}

export interface PlaidIdentityAccount {
  account_id: string;
  balances: AccountBalanceInfo;
  mask: string;
  name: string;
  official_name: string;
  owners: PlaidAccountOwner[] | null;
}
export interface PlaidIdentityResult {
  accounts: PlaidIdentityAccount[];
}

export interface BankInfo extends ModelBase  {
  plaidID: string;
  accountNumber: string;
  routingNumber: string;
  wireRoutingNumber: string;
  plaidItemID: string;
  plaidAccessToken: string;
  name: string;
  userID: string;
  accountOwner: AccountOwnerInfo;
  balances: Pick<AccountBalanceInfo, 'available' | 'current' | 'limit'>;
}
