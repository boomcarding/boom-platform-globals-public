import { Booking } from '.';
import { ModelBase } from './base-record';
import {
  DistanceUnit,
  MassUnit,
  RateAttributes,
  ShippingOrderStatus,
  ShipmentMethod,
  ShippoTrackingStatus,
  ShippoTransactionStatus,
  SignatureConfirmation,
  CODPaymentMethod,
  FedExAlcoholRecipientType,
  InsuranceProvider,
  UPSReturnServiceType,
  ShippoLabelType,
  ShippoRefundStatus,
  ShippoTransactionState,
  ShippoParcelResponseState,
} from './enums';
import { Money } from './money';

export interface Parcel {
  /**
   * this is the object_id in shippo's system
   */
  shippo_id?: string;
  length: number;
  width: number;
  height: number;
  weight: number;
  distance_unit: DistanceUnit;
  mass_unit: MassUnit;
  /**
   * This can be used later, not for MVP
   * extra can specify extra services, like signature required
   */
  extra?: object;
  /**
   * This can be used later, not for MVP
   * A string of up to 100 characters that can be filled with any
   * additional information you want to attach to the object.
   */
  metadata?: string;
  /**
   * This can be used later, not for MVP
   * A parcel template is a predefined package used by one
   * or multiple carriers. See the parcel template table for
   * all available values and the corresponding tokens. When
   * a template is given, the parcel dimensions do not have
   * to be sent. The dimensions below will instead be used.
   * The parcel weight is not affected by the use of a template.
   */
  template?: string;
}

export interface Rate {
  /**
   * unique id of given rate
   */
  shippo_id: string;
  /**
   * An array containing specific attributes of this Rate in context of the entire
   * shipment. Attributes can be assigned 'CHEAPEST', 'FASTEST', or 'BESTVALUE'
   */
  attributes: RateAttributes[];
  /**
   * Price of the shipment in shipper's currency
   */
  amount: Money;
  /**
   * carrier offering the rate. We won't be showing this but it could be useful
   */
  provider: string;
  /**
   * name of the service from the provider
   */
  service: string;
  /**
   * token for name of the service, this matches up with our ShipmentMethod enum
   */
  service_token: string;
  /**
   * estimated days
   */
  estimated_days: number;
  /**
   * Further clarification of the transit times. Often, this includes notes
   * that the transit time as given in "days" is only an average, not a guaranteed time.
   */
  duration_terms: string;
}

export interface ShippoTransaction {
  /**
   * Indicates the status of the Transaction
   */
  status: ShippoTransactionStatus;
  /**
   * Date and time of Transaction creation.
   */
  createdAt: number;
  /**
   * Date and time of last Transaction update.
   */
  updatedAt: number;
  /**
   * Unique Shippo identifier of the given Transaction object
   */
  shippo_id: string;
  /**
   * Shippo ID of the Rate object for which a Label has to be obtained.
   * Please note that only rates that were created less than 7 days ago
   * can be purchased in order to ensure up-to-date pricing
   */
  rate: string;
  /**
   * A string of up to 100 characters that can be filled with any additional
   * information you want to attach to the object.
   */
  metadata?: string;
  /**
   * The carrier-specific tracking number that can be used to track the
   * Shipment. A value will only be returned if the Rate is for a trackable
   * Shipment and if the Transactions has been processed successfully.
   */
  tracking_number?: string;
  /**
   * Indicates the high level status of the shipment
   */
  tracking_status?: ShippoTrackingStatus;
  /**
   * A link to track this item on the carrier-provided tracking website. A
   * value will only be returned if tracking is available and the carrier provides
   * such a service.
   */
  tracking_url_provider?: string;
  /**
   * The estimated time of arrival according to the carrier in utc unix
   */
  eta?: number;
  /**
   * A URL pointing directly to the label in the format you've set in your
   * settings. A value will only be returned if the Transactions has been processed
   * successfully.
   */
  label_url?: string;
}

/**
 * This matches a money total with a shipment method that the merchant will pay for
 */
export interface FreeShippingThreshold {
  /**
   * Amount of money a user has to spend to get free shipping
   */
  amountSpent: Money;
  /**
   * The shippo service (or self ship) that the merchant will pay for.
   */
  freeService: ShipmentMethod;
}

/**
 * Every product has a reference to a shipping policy
 */
export interface ShippingPolicy extends ModelBase {
  /**
   * The name of the policy, set by the merchant. Name will be used when a merchant is
   * selecting a policy to apply to a product
   */
  name: string;
  /**
   * The uid of the policy owner's boom account
   */
  merchantId: string;
  /**
   * a flat rate for shipping products if the merchant does not want to use our shipping solution. All products using
   * the same ShippingPolicy will combine to cost flatRate. As an example, if a user buys 2 different products that
   * have the same ShippingPolicy with flatRate = $5, the two items will cost a total of $5 to ship together
   */
  flatRate?: Money;
  /**
   * Items per flatRate. if flatRate = $1, itemsPerFlatRate = 5, and the user gets 10 items, shipping would cost $2
   */
  itemsPerFlatRate?: number;
  /**
   * the total a user has to spend to get free shipping. There can be several thresholds for different
   * shipping methods so this is a list
   *
   */
  freeShippingThresholds: FreeShippingThreshold[];
  /**
   * true if the product can't be shipped
   */
  pickUpOnly?: boolean;
  /**
   * a list of shippo_ids for the addresses the item can be picked up at
   */
  pickUpLocations?: string[];
}

/**
 * Each product would get a PackageDetails if it is using our shipping solution
 */
export interface PackageDetails {
  /**
   * The weight of the product
   */
  weight: number;
  /**
   * the mass unit of the weight
   */
  massUnit: MassUnit;
  /**
   * the _id of the box that this product ships in
   */
  boxId: string;
  /**
   * the number of items that can be shipped in the given box
   */
  itemsPerBox: number;
  /**
   * the shippo_id of the address this product ships from
   */
  shipsFrom: string;
}

/**
 * When a merchant adds a box to a product we save it so they can use it again later without
 * having to type in the box dimensions on every product
 */
export interface ShippingBox extends ModelBase {
  /**
   * uid of merchant who uses this box
   */
  merchantId: string;
  /**
   * name of the box, set by merchant for easy identification
   */
  name?: string;
  /**
   * unit that length, width, height are in
   */
  unit: DistanceUnit;
  /**
   * length of box in units
   */
  length: number;
  /**
   * width of box
   */
  width: number;
  /**
   * height of box
   */
  height: number;
}

/**
 * Each purchase transaction has a reference to a ShippingOrder. Transactions that are shipped
 * together would each refer to the same ShippingOrder
 */
export interface ShippingOrder extends ModelBase {
  /**
   * If the package got a label with Shippo the shippo id of that shippo transaction will be here
   */
  shippo_id?: string;
  /**
   * This is the same tracking number you could get with the shippo_id but it could also be
   * manually set by the merchant if they are not using shippo
   */
  trackingNumber?: string;
  /**
   * This is the link to a tracking page for the package
   */
  trackingLink?: string;
  /**
   * cost of the shipment is saved here
   */
  price: Money;
  /**
   * the uid of the purchaser
   */
  purchaser: string;
  /**
   * Either paid or refunded
   */
  status: ShippingOrderStatus;
}

/**
 * This is how rates are returned for lists of bookings
 */
export interface OrderGroup {
  /**
   * the name of the store that is seling/shipping this group of bookings
   */
  store: string;
  /**
   * The bookings that will be shipped together
   */
  bookings: Booking[];
  /**
   * if this is false, the bookings can only be picked up from the store
   */
  shippable: boolean;
  /**
   * This is the list of rates that are available for the listed bookings
   */
  rates: Rate[];
  /**
   * This is a utility to handle the user selected rate. Is need it to complete the purchase process.
   */
  selectedRate?: Rate;
}

export interface ShippingCheckoutRequest {
  /**
   * The list of bookings the user wants to purchase
   */
  bookings: Booking[];
  /**
   * The object_id of the address the user selected as the shipping address
   */
  shipToAddressId: string;
}

/**
 * An object holding optional extra services to be requested with a rate
 */
export interface ShipmentExtras {
  /**
   * Request standard or adult signature confirmation. You can alternatively request
   * Certified Mail (USPS only) or Indirect signature (FedEx only) or
   * Carrier Confirmation (Deutsche Post only)
   */
  signature_confirmation?: SignatureConfirmation;

  /**
   * Request “true” to give carrier permission to leave the parcel in a safe place if
   * no one answers the door (where supported). When set to “false”, if no one is available
   * to receive the item, the parcel will not be left (*surcharges may be applicable).
   */
  authority_to_leave?: boolean;

  /**
   * Marks shipment as to be delivered on a Saturday.
   */
  saturday_delivery?: boolean;

  /**
   *  Bypasses address validation (USPS, UPS, & LaserShip only).
   */
  bypass_address_validation?: boolean;

  /**
   *  Returns retail rates instead of account-based rates (UPS and FedEx only).
   */
  request_retail_rates?: boolean;

  /**
   *  Specify collection on delivery (DHL Germany, FedEx, GLS, OnTrac, and UPS only).
   */
  COD?: {
    /**
     * Amount to be collected.
     */
    amount: string;

    /**
     * Currency for the amount to be collected. Currently only USD is supported for FedEx and UPS.
     * ISO 4217 currency code (string)
     */
    currency: string;

    /**
     *  Secured funds include money orders, certified cheques and others (see UPS and FedEx for details).
     *  If no payment_method inputted the value defaults to "ANY".)
     */
    payment_method?: CODPaymentMethod;
  };

  /**
   *Indicates that a shipment contains Alcohol (Fedex and UPS only).
   */
  alcohol?: {
    /**
     * Mandatory for Fedex and UPS. Specifies that the package contains Alcohol.
     */
    contains_alcohol: boolean;

    /**
     * Mandatory for Fedex only. License type of the recipient of the Alcohol Package.
     */
    recipient_type?: FedExAlcoholRecipientType;
  };

  /**
   * Specifies that the package contains Dry Ice (FedEx and UPS only).
   */
  dry_ice?: {
    /**
     * Mandatory. Specifies that the package contains Dry Ice.
     */
    contains_dry_ice: boolean;

    /**
     *  Mandatory. Units must be in Kilograms. Cannot be greater than package weight.
     */
    weight: number;
  };

  /**
   *  Specify amount, content, provider, and currency of shipment insurance (UPS, FedEx and Ontrac only).
   */
  insurance?: {
    /**
     * Amount to be collected.
     */
    amount: number;

    /**
     *  Currency for the amount to be collected.
     */
    currency: string;

    /**
     *  Specify package content for insurance.
     */
    content: string;

    /**
     *  Specify the carrier insurance to have Insurance provided by the carrier directly,
     *  not by Shippo's 3rd-party Shipsurance insurance.
     */
    provider?: InsuranceProvider;
  };

  /**
   * This field specifies if it is a scan-based return shipment.
   * https://goshippo.com/docs/return-labels/
   */
  is_return?: boolean;

  /**
   * Optional text to be printed on the shipping label. Up to 50 characters.
   */
  reference_1?: string;

  /**
   * Optional text to be printed on the shipping label. Up to 50 characters.
   */
  reference_2?: string;

  /**
   * Request additional return option for return shipments (UPS only).
   */
  return_service_type?: UPSReturnServiceType;
}

/**
 * This is the request body for the POST /shipping/rates route
 */
export interface GetRatesRequest {
  /**
   * object_id of the destination AddressInfo
   */
  shipToAddressId: string;
  /**
   * object_id of the starting AddressInfo
   */
  shipFromAddressId: string;
  /**
   * List of Parcels to be shipped
   */
  parcels: Parcel[];
  /**
   * extra services to include in the rate
   */
  extra?: ShipmentExtras;
  /**
   * True if you want all possible rates returned. If this is missing or false
   * only 2-4 of the most relevant rates are returned
   */
  returnAll?: boolean;
  /**
   * Shipment methods that you want included in the list of rates. If this is provided, ONLY the methods listed
   * and the cheapest rate will be returned
   */
  shipmentMethods?: ShipmentMethod[];
}

/**
 * This is the request body for the POST /shipping/purchase route
 */
export interface PurchaseRateRequest {
  /**
   * The shippo id of the rate to purchase
   */
  shippoRateId: string;
  /**
   * the uid of the boom user that is paying for shipping
   */
  purchaserId: string;
  /**
   * The file type of the label to be created. The default can be set in our shippo dashboard,
   * this shouldn't need to be used
   */
  labelFileType?: ShippoLabelType;
}

/**
 * This is the request body for the POST /shipping/estimate route
 */
export interface EstimateRateRequest {
  /**
   * An example parcel to estimate rates for
   */
  parcel: Parcel;
  /**
   * the shippment method the user wants to estimate the cost for
   */
  shipmentMethod: ShipmentMethod;
  /**
   * object_id of a destination AddressInfo
   */
  to: string;
  /**
   * object_id of the AddressInfo the package would ship from
   */
  from: string;
}

/**
 * This is the request body for the POST /shipping/checkout route
 */
export interface ShippingCheckoutRequest {
  /**
   * The bookings a user is checking out with
   */
  bookings: Booking[];
  /**
   * The object_id of the chosen destination AddressInfo
   */
  shipToAddressId: string;
}

/**
 * This is the interface for the data returned by Shippo Addresses endpoints
 */
export interface ShippoAddressResponse {
  /**
   * Complete addresses contain all required values.
   *
   * Incomplete addresses have failed one or multiple validations.
   * Incomplete Addresses are eligible for requesting rates but lack at least one required value for purchasing labels.
   */
  is_complete: boolean;
  /**
   * Date and time of Address creation
   */
  object_created: string;
  /**
   * Date and time of last Address update. Since you cannot update Addresses after they were
   * created, this time stamp reflects the time when the Address was changed by Shippo's
   * systems for the last time, e.g., during the approximation of one or more values.
   */
  object_updated: string;
  /**
   *  Unique identifier of the given Address object. This ID is required to create a Shipment object.
   */
  object_id: string;
  /**
   *  Username of the user who created the Address object.
   */
  object_owner: string;
  /**
   * First and Last Name of the addressee (The person who will be getting the package at this address)
   */
  name?: string;
  /**
   * Company Name
   */
  company?: string;
  /**
   *  First street line, 35 character limit. Usually street number and street name (except for DHL Germany, see street_no).
   */
  street1: string;
  /**
   *  Street number of the addressed building. This field can be included in street1 for all carriers except for DHL Germany.
   */
  street_no?: string;
  /**
   *  Second street line, 35 character limit.
   */
  street2?: string;
  /**
   *  Third street line, 35 character limit. Only accepted for USPS international shipments, UPS domestic and UPS international shipments.
   */
  street3?: string;
  /**
   *  Name of a city. When creating a Quote Address, sending a city is optional but will yield more accurate Rates. Please
   *  bear in mind that city names may be ambiguous (there are 34 Springfields in the US). Pass in a state or a ZIP
   *  code (see below), if known, it will yield more accurate results.
   */
  city?: string;
  /**
   *  Postal code of an Address. When creating a Quote Addresses, sending a ZIP is optional but will yield more accurate Rates.
   */
  zip?: string;
  /**
   *  State/Province values are required for shipments from/to the US, AU, and CA. UPS requires province for some
   *  countries (i.e Ireland). To receive more accurate quotes, passing this field is recommended. Most carriers
   *  only accept two or three character state abbreviations.
   */
  state?: string;
  /**
   * Example: 'US' or 'DE'. All accepted values can be found on the Official ISO Website.
   * Sending a country is always required.
   * https://www.iso.org/home.html
   */
  country?: string;
  /**
   *  Addresses containing a phone number allow carriers to call the recipient when delivering
   * the Parcel. This increases the probability of delivery and helps to avoid accessorial
   * charges after a Parcel has been shipped.
   */
  phone?: string;
  /**
   * E-mail address of the contact person, RFC3696/5321-compliant.
   */
  email?: string;
  /**
   * Indicates whether the address provided is a residential address or not.
   */
  is_residential?: boolean;
  /**
   * A string of up to 100 characters that can be filled with any additional information you want to attach to the object.
   */
  metadata?: string;
  /**
   * Indicates whether the object has been created in test mode.
   */
  test?: boolean;
  /**
   * object that contains information regarding if an address had been validated or not.
   * Also contains any messages generated during validation.
   * Children keys are is_valid(boolean) and messages(array).
   */
  validation_results?: {
    is_valid?: boolean;
    messages?: string[];
  };
}

/**
 * This is the interface for the data returned from Shippo when a refund is requested
 */
export interface ShippoRefundResponse {
  /**
   * Date and time of object creation.
   */
  object_created: string;
  /**
   * Date and time of last object update.
   */
  object_updated: string;
  /**
   * Unique identifier of the given object.
   */
  object_id: string;
  /**
   * Username of the user who created the object.
   */
  object_owner: string;

  /**
   * Indicates the status of the Refund.
   */
  status: ShippoRefundStatus;
  /**
   * Object ID of the Transaction to be refunded.
   */
  transaction: string;
  /**
   * Indicates whether the object has been created in test mode.
   */
  test: boolean;
}

/**
 * Interface for messages from shippo attached to a response
 *  According to the shippo docs this has the following schema:
 * "code" (string): an identifier for the corresponding message (not always available")
 * "message" (string): a publishable message containing further information.
 *
 * in many tests I have seen the 'message' field is actually named 'text' so I included both.
 * I figure if we need to get these messages we can do something like messages[i].message || messages[i].text
 *
 * source is used in Rate response messages
 */
export interface ShippoResponseMessage {
  /**
   * Used in rate response messages. The provider who is returning a message
   */
  source?: string;
  /**
   * sometimes used by providers
   */
  code?: string;
  /**
   * The field with the message according to Shippo docs
   */
  message?: string;
  /**
   * The field that usually holds the message in my testing
   */
  text?: string;
}

/**
 * Interface for rates returned by shippo
 */
export interface ShippoRateResponse {
  /**
   *  Date and time of Rate creation.
   */
  object_created: string;
  /**
   *  Unique identifier of the given Rate object.
   */
  object_id: string;
  /**
   *  Username of the user who created the rate object.
   */
  object_owner: string;
  /**
   *  An array containing specific attributes of this Rate in context of the entire shipment.
   *  Attributes can be assigned 'CHEAPEST', 'FASTEST', or 'BESTVALUE'.
   */
  attributes: RateAttributes[];
  /**
   * Final Rate price, expressed in the currency used in the recipient's country.
   */
  amount_local: number;
  /**
   *  Currency used in the recipient's country, refers to "amount_local". The official
   *  ISO 4217 currency codes are used, e.g. "USD" or "EUR"
   */

  currency_local: string;
  /**
   * Final Rate price, expressed in the currency used in the sender's country.
   */
  amount: number;
  /**
   *  Currency used in the sender's country, refers to "amount". The official ISO 4217
   *  currency codes are used, e.g. "USD" or "EUR".
   */
  currency: string;
  /**
   * Carrier offering the rate, e.g., "FedEx" or "Deutsche Post DHL".
   */
  provider: string;
  /**
   *  URL to the provider logo with max. dimensions of 75*75px. Please refer to the
   *  provider's Logo Usage Guidelines before using the logo.
   */
  provider_image_75?: string;
  /**
   *  URL to the provider logo with max. dimensions of 200*200px. Please refer to
   *  the provider's Logo Usage Guidelines before using the logo.
   */
  provider_image_200?: string;
  /**
   * Contains details regarding the service level for the given rate.
   */
  servicelevel?: {
    /**
     *  Name of the Rate's servicelevel, e.g. "International Priority" or "Standard Post". A
     *  servicelevel commonly defines the transit time of a Shipment (e.g., Express vs. Standard),
     *  along with other properties. These names vary depending on the provider.
     */
    name: string;
    /**
     * Token of the Rate's servicelevel, e.g. "usps_priority" or "fedex_ground". See servicelevels.
     * https://goshippo.com/docs/reference/js#servicelevels
     */
    token: ShipmentMethod;
    /**
     *  Further clarification of the service.
     */
    terms: string;
  };
  /**
   * Estimated transit time (duration) in days of the Parcel at the given servicelevel. Please
   * note that this is not binding, but only an average value as given by the provider. Shippo
   * is not able to guarantee any transit times.
   */
  estimated_days: number;
  /**
   * Further clarification of the transit times. Often, this includes notes that the transit time
   * as given in "days" is only an average, not a guaranteed time.
   */
  duration_terms: string;
  /**
   *  Object ID of the carrier account that has been used to retrieve the rate.
   */
  carrier_account: string;
  /**
   *  The parcel's transit zone token. These tokens can vary depending on the provider.
   */
  zone?: string;
  /**
   * An array containing elements of the following schema:
   * "code" (string): an identifier for the corresponding message (not always available")
   * "message" (string): a publishable message containing further information.
   *
   * in many tests I have seen the 'message' field is actually named 'text' so I included both.
   * I figure if we need to get these messages we can do something like messages[i].message || messages[i].text
   */
  messages?: ShippoResponseMessage[];
  /**
   * Indicates whether the object has been created in test mode.
   */
  test?: boolean;
}

/**
 * Extra services that can be added to Parcels in the extra field of the Parcel object
 */
export interface ShippoParcelExtras {
  /**
   * Specify collection on delivery details (UPS, and FedEx only).
   */
  COD?: {
    /**
     *  Amount to be collected.
     */
    amount?: string;
    /**
     *  Currency for the amount to be collected. Currently only USD is supported for FedEx and UPS.
     */
    currency?: string;
    /**
     *  Secured funds include money orders, certified cheques and others (see UPS and FedEx for details).
     *  If no payment_method inputted the value defaults to "ANY".)
     */

    payment_method?: CODPaymentMethod;
  };
  /**
   *  Specify collection on delivery details (UPS, and FedEx only).
   */
  insurance?: {
    /**
     *  Amount to be collected.
     */
    amount?: string;
    /**
     *   Currency for the amount to be collected. Currently only USD is supported for FedEx and UPS.
     */
    currency?: string;
    /**
     *  Specify the carrier insurance to have Insurance provided by the carrier directly.
     */
    provider?: 'FEDEX' | 'UPS';
    /**
     *   Specify package content for insurance.
     */
    content?: string;
  };
}

/**
 * The interface for Parcel data returned from Shippo
 */
export interface ShippoParcelResponse {
  /**
   * Should be 'VALID'
   * A Parcel will only be valid when all required values have been sent and validated successfully.
   */
  object_state?: ShippoParcelResponseState;
  /**
   *  Date and time of Parcel creation.
   */
  object_created: string;
  /**
   *  Date and time of last Parcel update. Since you cannot update Parcels after they were created,
   * this time stamp reflects the time when the Parcel was changed by Shippo's systems for the last
   * time, e.g., during sorting the dimensions given.
   */
  object_updated: string;
  /**
   *  Unique identifier of the given Parcel object. This ID is required to create a Shipment object.
   */
  object_id: string;
  /**
   *  Username of the user who created the Parcel object.
   */
  object_owner: string;
  /**
   *  Length of the Parcel. Up to six digits in front and four digits after the decimal separator are accepted.
   */
  length: number;
  /**
   * Width of the Parcel. Up to six digits in front and four digits after the decimal separator are accepted.
   */
  width: number;
  /**
   *  Height of the parcel. Up to six digits in front and four digits after the decimal separator are accepted.
   */
  height: number;
  /**
   *  The unit used for length, width and height.
   */
  distance_unit: DistanceUnit;
  /**
   *  Weight of the parcel. Up to six digits in front and four digits after the decimal separator are accepted.
   */
  weight: number;
  /**
   *  The unit used for weight.
   */
  mass_unit: MassUnit;
  /**
   *  A parcel template is a predefined package used by one or multiple carriers. See the parcel template
   *  table for all available values and the corresponding tokens. When a template is given, the parcel
   *  dimensions do not have to be sent. The dimensions below will instead be used. The parcel weight is not
   *  affected by the use of a template.
   * https://goshippo.com/docs/reference/js#parcel-templates
   */
  template?: string;
  /**
   *  A string of up to 100 characters that can be filled with any additional information you want to attach to the object.
   */
  metadata?: string;
  /**
   *  An object holding optional extra services to be requested for each parcel in a multi-piece shipment.
   *  See ShippoParcelExtras for all available services.
   * These extras are seperate from Shippo shipment extras
   */
  extra?: ShippoParcelExtras;
  /**
   *  Indicates whether the object has been created in test mode.
   */
  test?: boolean;
}

/**
 * Interface for the data returned from the Shippo shipment endpoints
 */
export interface ShippoShipmentResponse {
  /**
   * "Waiting" shipments have been successfully submitted but not yet been processed. "Queued"
   *  shipments are currently being processed. "Success" shipments have been processed successfully,
   *  meaning that rate generation has concluded. "Error" does not occur currently and is reserved for future use.
   *
   *  We submit our shipments with 'async: false' so this should only ever be 'SUCCESS' for us
   */
  status: ShippoTransactionStatus;
  /**
   *  Date and time of Shipment creation.
   */
  object_created: string;
  /**
   *  Date and time of last Shipment update.
   */
  object_updated: string;
  /**
   *  Unique identifier of the given Shipment object.
   */
  object_id: string;
  /**
   *  Username of the user who created the Shipment object.
   */
  object_owner: string;
  /**
   *  Address object that should be used as sender Address.
   */
  address_from: ShippoAddressResponse;
  /**
   *  Address object that should be used as recipient Address.
   */
  address_to: ShippoAddressResponse;
  /**
   * Array of Parcel objects to be shipped.
   */
  parcels: ShippoParcelResponse[];
  /**
   *  Date the shipment will be tendered to the carrier. Must be in the format "2014-01-18T00:35:03.463Z".
   *  Defaults to current date and time if no value is provided. Please note that some carriers require
   *  this value to be in the future, on a working day, or similar.
   */
  shipment_date?: string;
  /**
   * Address object where the shipment will be sent back to if it is not delivered (Only available for UPS, USPS, and Fedex shipments).
   * If this field is not set, your shipments will be returned to the address_from
   */
  address_return?: ShippoAddressResponse;
  /**
   *  A string of up to 100 characters that can be filled with any additional information you want to attach to the object.
   */
  metadata?: string;
  /**
   * An object holding optional extra services to be requested. See the Shipment Extra table below for all available services.
   * https://goshippo.com/docs/reference/js#shipments-extras
   */
  extra?: ShipmentExtras;
  /**
   *  An array with all available rates. If async has been set to false in the request, this will be populated with all
   *  available rates in the response. Otherwise rates will be created asynchronously and this array will initially be empty.
   */
  rates: ShippoRateResponse[];
  /**
   *  Shippo api documentation says:
   *  	An array containing elements of the following schema:
   * "code" (string): an identifier for the corresponding message (not always available")
   * "message" (string): a publishable message containing further information.
   *
   * From testing it looks like the actual schema is:
   *  source: provider who is returning a message
   *  code: message id
   *  text: text of the message
   *
   */
  messages?: ShippoResponseMessage[];
  /**
   * Indicates whether the object has been created in test mode.
   */
  test?: boolean;
}

/**
 * the data returned from Shippo transaction routes
 */
export interface ShippoTransactionResponse {
  /**
   *  Indicates the validity of the Transaction object based on the given data, regardless of what the corresponding carrier returns.
   */
  object_state: ShippoTransactionState;
  /**
   *  Indicates the status of the Transaction.
   */
  status: ShippoTransactionStatus;
  /**
   *  Date and time of Transaction creation.
   */
  object_created: string;
  /**
   *  Date and time of last Transaction update.
   */
  object_updated: string;
  /**
   *  Unique identifier of the given Transaction object.
   */
  object_id: string;
  /**
   *  Username of the user who created the Transaction object.
   */
  object_owner: string;
  /**
   * ID of the Rate object for which a Label has to be obtained. Please note that only rates that are not older
   * than 7 days can be purchased in order to ensure up-to-date pricing.
   */
  rate: string;
  /**
   *  A string of up to 100 characters that can be filled with any additional information you want to attach to the object.
   */
  metadata?: string;
  /**
   *  Specify the label file format for this label. If you don't specify this value, the API will default to
   *  your default file format that you can set on the settings page.
   *  See supported label formats per carrier here:
   *    https://goshippo.com/docs/carriers/
   */
  label_file_type?: ShippoLabelType;
  /**
   *  Indicates whether the object has been created in test mode.
   */
  test?: boolean;
  /**
   *  The carrier-specific tracking number that can be used to track the Shipment. A value will only be
   *  returned if the Rate is for a trackable Shipment and if the Transactions has been processed successfully.
   */
  tracking_number?: string;
  /**
   *  Indicates the high level status of the shipment: 'UNKNOWN', 'DELIVERED', 'TRANSIT', 'FAILURE', 'RETURNED'.
   */
  tracking_status?: ShippoTrackingStatus;
  /**
   *  A link to track this item on the carrier-provided tracking website. A value will only be returned if
   *  tracking is available and the carrier provides such a service.
   */
  tracking_url_provider?: string;
  /**
   *  The estimated time of arrival according to the carrier.
   */
  eta?: string;
  /**
   *  A URL pointing directly to the label in the format you've set in your settings. A value will only
   *  be returned if the Transactions has been processed successfully.
   */
  label_url?: string;
  /**
   *  A URL pointing to the commercial invoice as a 8.5x11 inch PDF file. A value will only be returned if
   *  the Transactions has been processed successfully and if the shipment is international.
   */
  commercial_invoice_url?: string;
  /**
   * An array containing elements of the following schema:
   * "code" (string): an identifier for the corresponding message (not always available")
   * "message" (string): a publishable message containing further information.
   *
   * in many tests I have seen the 'message' field is actually named 'text' so I included both.
   * I figure if we need to get these messages we can do something like messages[i].message || messages[i].text
   */
  messages?: ShippoResponseMessage[];
  /**
   *  A URL pointing directly to the QR code in PNG format. A value will only be returned if requested
   *  using qr_code_requested flag and the carrier provides such an option.
   */
  qr_code_url?: string;
}
