import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from 'axios';

let api: AxiosInstance;
let errorHandler: Function | null = (error: Error) => {
  console.log(error);
};

export const initializeSearchAPI = (
  url: string,
  {
    username,
    password,
    APIKey,
    timeout = 10000,
  }: { username: string; password: string; APIKey: string; timeout: number }
) => {
  api = axios.create({
    baseURL: url,
    timeout,
    headers: {
      Accept: 'application/json',
      ...(APIKey ? { Authorization: APIKey } : {}),
    },
    ...(!APIKey
      ? {
          auth: {
            username,
            password,
          },
        }
      : {}),
  } as AxiosRequestConfig);

  // Add a response interceptor.
  api.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      errorHandler!(error);
      return Promise.reject(error);
    }
  );
};

export const onSearchAPIError = (handleError: Function) => {
  errorHandler = handleError;
  return () => (errorHandler = null);
};

export const query = async <T = any, R = AxiosResponse<T>>(
  index: string,
  data: any,
  config?: AxiosRequestConfig
): Promise<R> => {
  return api.post(`${index}/_search`, data, config);
};
