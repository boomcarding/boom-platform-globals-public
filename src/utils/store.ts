import { AddressInfo } from '../address-info';

export const getComposedAddressFromStore = (
  store: AddressInfo | undefined,
  excludedFields: string[] = []
): string => {
  if (store) {
    const address: string[] = [];

    if (!excludedFields.includes('number') && store.number !== undefined)
      address.push(store.number);

    if (!excludedFields.includes('street1') && store.street1 !== undefined)
      address.push(store.street1);

    if (!excludedFields.includes('street2') && store.street2 !== undefined)
      address.push(store.street2);

    return address.join(', ');
  } else {
    return '';
  }
};
