//@ts-ignore
import Dinero, { Currency, Dinero } from 'dinero.js';
import numeral from 'numeral';
import { Money } from '../money';

export const toMoney = (
  // TODO: Rename to amountToMoney
  amount: number | string,
  currency: Currency = 'USD',
  symbol: string = '$'
): Money => {
  const normalizedAmount = numeral(amount).format('00.00');
  const amountInteger = parseInt(normalizedAmount.replace('.', ''));
  return { amount: amountInteger, precision: 2, currency, symbol };
};

export const fromMoney = (
  // TODO: Rename to moneyToDinero
  money?: Money,
  includeSymbol: boolean = true,
  customFormatting: string = '0,0.00'
): string => {
  try {
    // TODO: review if money.amount is a number, there are some records returning this [amount: "acv"]
    return Dinero(money || { amount: 0, precision: 2 }).toFormat(
      `${money && includeSymbol ? money.symbol || '$' : ''}${customFormatting}`
    );
  } catch (error) {
    console.error('Error converting Money value', error);
    return 'invalid';
  }
};

export const greaterThanOrEqual = (money1: Money, money2: Money): boolean => {
  return Dinero(money1).greaterThanOrEqual(Dinero(money2));
};

export const dineroToMoney = (dinero: Dinero.Dinero, symbol?: string): Money => {
  return { ...dinero.toObject(), symbol: symbol || '$' } as Money;
};
