/**
 * Used to get environment variables parsed correctly
 * @param {string} value - variable from the .env file
 * @returns {boolean}
 */

export const processEnvBoolean = (value: undefined | string | boolean): boolean => {
  switch (value) {
    case 'false':
    case '0':
    case false:
      return false;
    default:
      return true;
  }
};
