import { Offer } from '../models/offer.model';
import { Product } from '../models/product.model';
import { Store, StoreBasic } from '../models/store.model';
import { BoomUserBasic } from '../user';

export const isOffer = (item?: Partial<Product> | Partial<Offer>): item is Offer => {
  return (
    !!item &&
    !!(item as Offer).expiration &&
    !!(item as Offer).startDate &&
    !!(item as Offer).product
  );
};

export const isArray = (item: any): item is any[] => {
  return Array.isArray(item);
};

export const isStore = (item: Partial<Store> | any): item is StoreBasic => {
  return !!item && !!(item as StoreBasic).companyName;
};

export const isBoomUser = (item: BoomUserBasic | any): item is BoomUserBasic => {
  return (
    !!item &&
    !!(item as BoomUserBasic).uid &&
    !!(item as BoomUserBasic).firstName &&
    !!(item as BoomUserBasic).lastName
  );
};

export const isProduct = (item?: Partial<Product> | Partial<Offer>): item is Product => {
  return (
    !!item &&
    !!(item as Product).name &&
    !!(item as Product).category &&
    !!(item as Product).merchantUID &&
    !!(item as Product).price &&
    !!(item as Product).status &&
    !!(item as Product).store
  );
};
