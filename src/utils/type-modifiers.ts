/**
 * Makes all optional except the list passed as second argument
 * sample : type NewType = AllOptionalExceptFor<SomeType, 'prop1' | 'prop2'>
 * Old method(doesn't allow empty list of optional) : export type AllOptionalExceptFor<T, TRequired extends keyof T> = Partial<T> & Pick<T, TRequired>;
 * */
export type AllOptionalExceptFor<T, TRequired extends keyof T = keyof T> = Partial<
  Pick<T, Exclude<keyof T, TRequired>>
> &
  Required<Pick<T, TRequired>>;

/**
 * Makes all required
 * sample : type NewType = AllRequiredExceptFor<SomeType, 'prop1' | 'prop2'>
 */
type Diff<T, U> = T extends U ? never : T;

export type AllRequiredExceptFor<T, TOptional extends keyof T> = Pick<T, Diff<keyof T, TOptional>> &
  Partial<T>;
/**
 * Makes all optional
 * sample : interface BoomUserForUpdates extends AllOptional<BoomUser> {}
 */
export type AllOptional<T> = { [K in keyof T]?: T[K] };
