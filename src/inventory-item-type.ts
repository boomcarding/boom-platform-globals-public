import { Money } from './money';

export interface InventoryItemType {
  label: string;
  name: string;
  purchasePrice: Money;
  count: number;
}
