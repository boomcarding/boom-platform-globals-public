import { ModelBase } from ".";
export interface UserBankAccountInfo {
    relatedRecordId?: string;
    accountName: string;
    accountBalance: AccountBalanceInfo;
    accountNumber: PlaidAuthAccountNumbers;
    memberId?: string;
    memberFullName: string;
    memberAddress: string;
    memberPhoneNumber: string;
}
export interface BoomUserPlaidInfo {
    account: {
        id: any;
        mask: any;
        name: any;
        subtype: any;
        type: any;
    };
    account_id: any | null;
    accounts: BoomUserPlaidAccount[];
    institution: {
        institution_id: string;
        name: string;
    };
    item: {
        accessToken: string;
        itemId: string;
    };
    link_session_id: string;
    public_token: string;
}
export interface BoomUserPlaidAccount {
    id: string;
    mask: string;
    name: string;
    subtype: string;
    type: string;
}
export interface PlaidAuthAccountNumberInfo {
    account: string;
    account_id: string;
    routing: string;
    wire_routing: string;
}
export interface PlaidAuthAccountNumbers {
    achNumbers: PlaidAuthAccountNumberInfo[];
    eftNumbers: PlaidAuthAccountNumberInfo[];
    internationalNumbers: PlaidAuthAccountNumberInfo[];
    bacsNumbers: PlaidAuthAccountNumberInfo[];
}
export interface AccountBalanceInfo {
    available: number;
    current: number;
    iso_currency_code: string;
    limit: number | null;
    unofficial_currency_code: string | null;
}
export interface PlaidAuthResult {
    numbers: PlaidAuthAccountNumbers;
    accounts: any;
}
export interface UserBankInfo {
    userPhoneNumber: string;
    userFullName: string;
    userId?: string;
    institutions: BankInstitution[];
}
export interface BankInstitution {
    bankName: string;
    accounts: BankAccount[];
}
export interface BankAccount {
    accountholderName: string;
    accountholderAddress: string;
    accountType: string;
    accountNumber: string;
    routingNumber: string;
    wireRoutingNumber: string;
    accountID: string;
    balance: AccountBalanceInfo;
}
export interface AccountOwnerInfo {
    phone: string;
    names: string[];
    address: string;
    city: string;
    state: string;
    zip: string;
    emails: string[];
    gotInfoFromBank: boolean;
}
export interface PlaidAddress {
    data: {
        city: string;
        country: string;
        postal_code: string | null;
        region: string | null;
        street: string;
    };
    primary: boolean | null;
}
export interface PlaidAccountOwner {
    addresses: PlaidAddress[];
    emails: {
        data: string;
        primary: boolean;
        type: string;
    }[];
    names: string[];
    phone_numbers: {
        data: string;
        primary: boolean | null;
        type: string | null;
    }[];
}
export interface PlaidIdentityAccount {
    account_id: string;
    balances: AccountBalanceInfo;
    mask: string;
    name: string;
    official_name: string;
    owners: PlaidAccountOwner[] | null;
}
export interface PlaidIdentityResult {
    accounts: PlaidIdentityAccount[];
}
export interface BankInfo extends ModelBase {
    plaidID: string;
    accountNumber: string;
    routingNumber: string;
    wireRoutingNumber: string;
    plaidItemID: string;
    plaidAccessToken: string;
    name: string;
    userID: string;
    accountOwner: AccountOwnerInfo;
    balances: Pick<AccountBalanceInfo, 'available' | 'current' | 'limit'>;
}
