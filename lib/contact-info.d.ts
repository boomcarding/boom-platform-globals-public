export interface ContactInfo {
    emails?: string[];
    phoneNumber?: string;
}
