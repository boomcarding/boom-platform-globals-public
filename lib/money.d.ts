import { Options, Currency } from 'dinero.js';
export interface Money extends Options {
    amount: number;
    precision: number;
    currency: Currency;
    symbol: string;
}
