import { ContactInfo } from './contact-info';
import { AddressInfo } from './address-info';
import { Money } from './money';
import { RoleKey } from './enums';
import { Store } from './models/store.model';
import { BoomUserPlaidInfo } from './bank-info';
import { Nexus } from './sales-tax';
export interface BoomUserBasic {
    uid?: string;
    firstName?: string;
    lastName?: string;
    contact?: ContactInfo;
    addresses?: AddressInfo[];
}
export interface ProfileImage {
    imgUrl?: string;
    imgFile?: File | null;
    base64Data?: string | null;
    previewImgUrl?: string | null;
    previewBase64Data?: string | null;
}
export interface BoomUser extends BoomUserBasic {
    createdAt?: number;
    updatedAt?: number;
    gender?: string;
    registrationStep?: number;
    finishedRegistration?: boolean;
    roles?: RoleKey[];
    cards?: string[] | undefined | null;
    store?: Partial<Store>;
    profileImg?: ProfileImage;
    enableNotification?: boolean;
    notificationSound?: boolean;
    notificationVibration?: boolean;
    fcmToken?: string;
    range?: number;
    grossEarningsPendingWithdrawal?: Money;
    netEarningsPendingWithdrawal?: Money;
    tempPassword?: string;
    password?: string;
    plaidInfo?: BoomUserPlaidInfo[];
    hasCards?: boolean;
    taxableNexus?: Nexus[];
    boomAccounts?: string[];
    forceUpdate?: boolean;
}
export declare enum AccountInfoQueryTypes {
    BoomcardPin = "boom-card-pin",
    UsernamePassword = "username-password"
}
