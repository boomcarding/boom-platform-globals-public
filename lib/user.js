"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountInfoQueryTypes = void 0;
var AccountInfoQueryTypes;
(function (AccountInfoQueryTypes) {
    AccountInfoQueryTypes["BoomcardPin"] = "boom-card-pin";
    AccountInfoQueryTypes["UsernamePassword"] = "username-password";
})(AccountInfoQueryTypes = exports.AccountInfoQueryTypes || (exports.AccountInfoQueryTypes = {}));
//# sourceMappingURL=user.js.map