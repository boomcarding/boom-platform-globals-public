import { BoomUserBasic } from '../user';
import { StoreBasic } from './store.model';
import { InventoryItemStatus, InventoryItemInactiveReason } from '../enums';
import { Money } from '../money';
import { ModelBase } from '../base-record';
import { AllOptionalExceptFor } from '../utils/type-modifiers';
export interface InventoryItem extends ModelBase {
    friendlyID?: string;
    itemID?: string;
    itemType?: string;
    itemName?: string;
    nickname?: string;
    merchant?: AllOptionalExceptFor<BoomUserBasic, 'uid'>;
    store?: AllOptionalExceptFor<StoreBasic, '_id'>;
    status?: InventoryItemStatus;
    purchasePrice?: Money;
    inactiveReason?: InventoryItemInactiveReason;
}
