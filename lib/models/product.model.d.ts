import { ModelBase } from '../base-record';
import { Category } from './category.model';
import { ProductStatus } from '../enums';
import { Money } from '../money';
import { PackageDetails } from '../shipping';
import { Store } from './store.model';
export interface Product extends ModelBase {
    imageUrl?: string;
    category?: Category;
    name?: string;
    description?: string;
    store?: Partial<Store>;
    price: Money;
    attributes?: object;
    _tags?: string[];
    merchantUID?: string;
    packageDetails?: PackageDetails;
    shippingPolicy: string;
    status: ProductStatus;
    quantity: number;
    returnPolicy: string;
}
