import { ModelBase } from '../base-record';
import { BoomUser } from '../user';
import { ContactInfo } from '../contact-info';
import { AddressInfo } from '../address-info';
import { Geolocation } from '../geolocation';
import { StoreTypes } from '../enums';
import { AllOptionalExceptFor } from '../utils/type-modifiers';
export interface StoreBasic extends ModelBase, ContactInfo, AddressInfo {
    companyName: string;
}
export interface Store extends StoreBasic {
    pin?: number;
    companyLogoUrl?: string;
    coverImageUrl?: string;
    companyType?: string;
    companyDescription?: string;
    fein?: number;
    years?: number;
    storeType?: StoreTypes;
    links?: string[];
    _tags?: string[];
    _geoloc: Geolocation;
    openingTime?: number;
    closingTime?: number;
    days?: string[];
    merchant?: AllOptionalExceptFor<BoomUser, 'uid' | 'firstName' | 'lastName'>;
}
