import { ModelBase } from '../base-record';
import { Transaction } from './transaction.model';
import { AllOptionalExceptFor } from '../utils';
export interface CustomerBilling extends ModelBase {
    transaction: AllOptionalExceptFor<Transaction, '_id'>;
    plaidAccountId?: string;
    plaidItemId?: string;
}
