import { ModelBase } from '../base-record';
import { InventoryItem } from './inventory-item.model';
import { Money } from '../money';
import { BoomUserBasic } from '../user';
import { FulfillmentStatus } from '../enums';
import { AllOptionalExceptFor } from '../utils/type-modifiers';
import { StoreBasic } from './store.model';
export interface InventoryLease extends ModelBase {
    lastChargedAt: number;
    inventoryItem: Pick<InventoryItem, '_id' | 'itemID' | 'itemName' | 'friendlyID' | 'itemType'>;
    leaseAmount: Money;
    leaseExpiration: number;
    fulfillmentAmount: Money;
    amountPaid: Money;
    merchant: AllOptionalExceptFor<BoomUserBasic, 'uid'>;
    store: AllOptionalExceptFor<StoreBasic, '_id'>;
    fulfillmentStatus: FulfillmentStatus;
}
