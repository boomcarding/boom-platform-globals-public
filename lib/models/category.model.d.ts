import { ModelBase } from '../base-record';
export interface Category extends ModelBase {
    name: string;
    commissionRate?: number;
    subCategories?: string[];
}
