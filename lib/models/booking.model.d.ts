import { ModelBase } from '../base-record';
import { Offer } from './offer.model';
import { Product } from './product.model';
import { BookingStatus, BookingTypes } from '../enums';
export interface Booking extends ModelBase {
    type: BookingTypes;
    item: Offer | Product;
    quantity: number;
    status: BookingStatus;
    memberUID: string;
    visits: number;
}
