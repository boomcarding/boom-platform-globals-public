import { ModelBase } from '../base-record';
import { AdminConfigType } from '../enums';
export interface Config extends ModelBase {
    type: AdminConfigType;
    label: string;
    value: {
        [key: string]: any;
    };
}
