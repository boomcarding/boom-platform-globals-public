import { ModelBase } from '../base-record';
import { Money } from '../money';
import { Product } from './product.model';
export interface Offer extends ModelBase {
    cashBackPerVisit?: Money;
    conditions?: string[];
    description?: string;
    maxQuantity?: number;
    maxVisits?: number;
    merchantUID?: string;
    startDate?: number;
    title?: string;
    product: Product;
    expiration?: number;
    returnPolicy?: string;
}
