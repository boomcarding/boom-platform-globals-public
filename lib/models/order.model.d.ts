import { ShippingOrder } from '..';
import { AddressInfo } from '../address-info';
import { ModelBase } from '../base-record';
import { OrderGroup } from '../shipping';
import { Transaction } from './transaction.model';
export interface Order extends ModelBase {
    orderGroups: OrderGroup[];
    shipToAddress: AddressInfo;
    customerUID: string;
    transactions?: Transaction[];
}
export interface MerchantOrder extends ModelBase {
    transactions: Transaction[];
    shippingOrder: ShippingOrder;
}
