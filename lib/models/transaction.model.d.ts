import { ModelBase } from '../base-record';
import { Booking } from './booking.model';
import { TransactionStatus, TransactionType } from '../enums';
import { Money } from '../money';
import { Offer } from './offer.model';
import { Product } from './product.model';
import { TaxCode } from '../sales-tax';
import { Store } from './store.model';
import { BoomUser } from '../user';
import { AllOptionalExceptFor } from '../utils/type-modifiers';
export interface Transaction extends ModelBase {
    type?: TransactionType;
    amount?: Money;
    cashback?: Money;
    nonce?: string;
    sender?: AllOptionalExceptFor<BoomUser, 'uid'> | AllOptionalExceptFor<Store, '_id'>;
    receiver?: AllOptionalExceptFor<BoomUser, 'uid'> | AllOptionalExceptFor<Store, '_id'>;
    title?: string;
    status?: TransactionStatus;
    taxcode?: TaxCode;
    salestax?: Money;
    purchaseItem?: Partial<Offer> | Partial<Product>;
    boomcardSuffix?: string;
    booking?: Partial<Booking>;
    dateReceived?: number;
    commissionCollected?: Money;
    shippingOrderId?: string;
    shortId: string;
}
