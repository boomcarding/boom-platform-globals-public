import { ModelBase } from './base-record';
import { Money } from './money';
import { BoomUserBasic } from './user';
import { Store } from './models/store.model';
import { InventoryItem } from './models/inventory-item.model';
import { InventoryLease } from './models/inventory-lease.model';
import { MerchantTransactionStatus, MerchantTransactionType } from './enums';
import { AllOptionalExceptFor } from './utils/type-modifiers';
export interface MerchantTransaction extends ModelBase {
    title: string;
    status: MerchantTransactionStatus;
    type: MerchantTransactionType;
    salestax?: Money;
    salestaxState?: string;
    amount?: Money;
    merchant: AllOptionalExceptFor<BoomUserBasic, 'uid'>;
    store: AllOptionalExceptFor<Store, '_id'>;
    purchaseItem: AllOptionalExceptFor<InventoryItem, '_id' | 'itemID' | 'itemName'>;
    inventoryLease?: AllOptionalExceptFor<InventoryLease, '_id'>;
}
