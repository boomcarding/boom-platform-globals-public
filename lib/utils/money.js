"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dineroToMoney = exports.greaterThanOrEqual = exports.fromMoney = exports.toMoney = void 0;
const dinero_js_1 = __importDefault(require("dinero.js"));
const numeral_1 = __importDefault(require("numeral"));
exports.toMoney = (amount, currency = 'USD', symbol = '$') => {
    const normalizedAmount = numeral_1.default(amount).format('00.00');
    const amountInteger = parseInt(normalizedAmount.replace('.', ''));
    return { amount: amountInteger, precision: 2, currency, symbol };
};
exports.fromMoney = (money, includeSymbol = true, customFormatting = '0,0.00') => {
    try {
        return dinero_js_1.default(money || { amount: 0, precision: 2 }).toFormat(`${money && includeSymbol ? money.symbol || '$' : ''}${customFormatting}`);
    }
    catch (error) {
        console.error('Error converting Money value', error);
        return 'invalid';
    }
};
exports.greaterThanOrEqual = (money1, money2) => {
    return dinero_js_1.default(money1).greaterThanOrEqual(dinero_js_1.default(money2));
};
exports.dineroToMoney = (dinero, symbol) => {
    return Object.assign(Object.assign({}, dinero.toObject()), { symbol: symbol || '$' });
};
//# sourceMappingURL=money.js.map