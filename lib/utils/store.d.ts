import { AddressInfo } from '../address-info';
export declare const getComposedAddressFromStore: (store: AddressInfo | undefined, excludedFields?: string[]) => string;
