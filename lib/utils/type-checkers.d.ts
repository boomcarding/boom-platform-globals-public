import { Offer } from '../models/offer.model';
import { Product } from '../models/product.model';
import { Store, StoreBasic } from '../models/store.model';
import { BoomUserBasic } from '../user';
export declare const isOffer: (item?: Partial<Offer> | Partial<Product> | undefined) => item is Offer;
export declare const isArray: (item: any) => item is any[];
export declare const isStore: (item: Partial<Store> | any) => item is StoreBasic;
export declare const isBoomUser: (item: BoomUserBasic | any) => item is BoomUserBasic;
export declare const isProduct: (item?: Partial<Offer> | Partial<Product> | undefined) => item is Product;
