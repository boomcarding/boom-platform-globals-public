export declare type AllOptionalExceptFor<T, TRequired extends keyof T = keyof T> = Partial<Pick<T, Exclude<keyof T, TRequired>>> & Required<Pick<T, TRequired>>;
declare type Diff<T, U> = T extends U ? never : T;
export declare type AllRequiredExceptFor<T, TOptional extends keyof T> = Pick<T, Diff<keyof T, TOptional>> & Partial<T>;
export declare type AllOptional<T> = {
    [K in keyof T]?: T[K];
};
export {};
