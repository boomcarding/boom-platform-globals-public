"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getComposedAddressFromStore = void 0;
exports.getComposedAddressFromStore = (store, excludedFields = []) => {
    if (store) {
        const address = [];
        if (!excludedFields.includes('number') && store.number !== undefined)
            address.push(store.number);
        if (!excludedFields.includes('street1') && store.street1 !== undefined)
            address.push(store.street1);
        if (!excludedFields.includes('street2') && store.street2 !== undefined)
            address.push(store.street2);
        return address.join(', ');
    }
    else {
        return '';
    }
};
//# sourceMappingURL=store.js.map