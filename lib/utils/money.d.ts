import { Currency } from 'dinero.js';
import { Money } from '../money';
export declare const toMoney: (amount: number | string, currency?: Currency, symbol?: string) => Money;
export declare const fromMoney: (money?: Money | undefined, includeSymbol?: boolean, customFormatting?: string) => string;
export declare const greaterThanOrEqual: (money1: Money, money2: Money) => boolean;
export declare const dineroToMoney: (dinero: any, symbol?: string | undefined) => Money;
