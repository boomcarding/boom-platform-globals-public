"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processEnvBoolean = void 0;
exports.processEnvBoolean = (value) => {
    switch (value) {
        case 'false':
        case '0':
        case false:
            return false;
        default:
            return true;
    }
};
//# sourceMappingURL=misc.js.map