"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isProduct = exports.isBoomUser = exports.isStore = exports.isArray = exports.isOffer = void 0;
exports.isOffer = (item) => {
    return (!!item &&
        !!item.expiration &&
        !!item.startDate &&
        !!item.product);
};
exports.isArray = (item) => {
    return Array.isArray(item);
};
exports.isStore = (item) => {
    return !!item && !!item.companyName;
};
exports.isBoomUser = (item) => {
    return (!!item &&
        !!item.uid &&
        !!item.firstName &&
        !!item.lastName);
};
exports.isProduct = (item) => {
    return (!!item &&
        !!item.name &&
        !!item.category &&
        !!item.merchantUID &&
        !!item.price &&
        !!item.status &&
        !!item.store);
};
//# sourceMappingURL=type-checkers.js.map