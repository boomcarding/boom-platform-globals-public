export interface AddressInfo {
    object_id?: string;
    is_complete?: boolean;
    name?: string;
    number?: string;
    street1?: string;
    street2?: string;
    city?: string;
    state?: string;
    zip?: string;
    country?: string;
}
export interface AddressValidationResponse {
    address?: AddressInfo;
    is_valid: boolean;
    messages?: string[];
}
