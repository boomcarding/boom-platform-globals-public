export interface Geolocation {
    lat: number | null;
    lng: number | null;
}
