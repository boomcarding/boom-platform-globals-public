export interface ModelBase {
    _id?: string;
    createdAt?: number;
    updatedAt?: number;
}
