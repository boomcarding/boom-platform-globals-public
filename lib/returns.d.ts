import { ModelBase } from './base-record';
import { ReturnReason, ReturnMethod, TransactionTotalParts, ReturnCostType, Status } from './enums/returns';
import { Money } from './money';
export interface ReturnPolicy extends ModelBase {
    merchantID: string;
    name: string;
    description: string;
    refundsAccepted: boolean;
    autoApprove: boolean;
    costsImposed?: ExtraCosts[];
    daysToReturn: number;
    returnMethod: ReturnMethod;
    dropOffAddress?: string[];
    transactionTotalPartsToRefund?: TransactionTotalParts[];
    returnCosts: ReturnCost[];
}
export interface ExtraCosts {
    name: string;
    description: string;
    price: Money;
}
export interface ReturnCost extends ExtraCosts {
    type: ReturnCostType;
}
export interface ReturnRequest extends ModelBase {
    customerID: string;
    merchantID: string;
    refundStatus?: Status;
    returnStatus: Status;
    merchantPolicyID: string;
    returnReason: ReturnReason[];
    customReason?: string;
    returnMethod: ReturnMethod;
    purchaseTransactionID: string;
    refundAmount?: Money;
    returnTransactionID: string;
    comment?: string;
}
export interface ReturnDispute extends ModelBase {
    returnRequest: ReturnRequest;
    isOpen: boolean;
    comment: string;
}
export interface Message {
    text: string;
    fromID: string;
    toID: string;
}
