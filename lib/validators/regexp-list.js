"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhoneRegex = exports.PhoneRegex2 = exports.PriceRegex = exports.EmailRegex = exports.ShippingLabelFileTypeRegex = exports.CountryUpperOrLowerRegex = exports.MassUnitRegex = exports.DistanceUnitRegex = exports.ZipCodeRegex = exports.ShipmentMethodsRegex = exports.AcceptedCurrencySymbolRegex = exports.AcceptedCurrencyRegex = exports.StatesUpperOrLowerRegex = exports.TaxRegionsRegex = exports.TaxCountriesRegex = void 0;
exports.TaxCountriesRegex = /^(US)$/;
exports.TaxRegionsRegex = /^(AK|AL|AR|AS|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY)$/;
exports.StatesUpperOrLowerRegex = /^(AK|AL|AR|AS|AZ|CA|CO|CT|DC|DE|FL|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY|ak|al|ar|as|az|ca|co|ct|dc|de|fl|ga|gu|hi|ia|id|il|in|ks|ky|la|ma|md|me|mi|mn|mo|ms|mt|nc|nd|ne|nh|nj|nm|nv|ny|oh|ok|or|pa|pr|ri|sc|sd|tn|tx|ut|va|vi|vt|wa|wi|wv|wy)$/;
exports.AcceptedCurrencyRegex = /^USD$/;
exports.AcceptedCurrencySymbolRegex = /^\$$/;
exports.ShipmentMethodsRegex = /^(usps_priority|usps_priority_express|usps_first|usps_parcel_select|ups_standard|ups_ground|ups_saver|ups_3_day_select|ups_second_day_air|ups_second_day_air_am|ups_next_day_air|ups_next_day_air_saver|ups_next_day_air_early_am|ups_mail_innovations_domestic|ups_surepost|ups_surepost_bound_printed_matter|ups_surepost_lightweight|ups_surepost_media|ups_express|ups_express_1200|ups_express_plus|ups_expedited|self ship)$/;
exports.ZipCodeRegex = /^(\d{5}(?:-\d{4})?)$/;
exports.DistanceUnitRegex = /^(cm|ft|in|m|yd|mm)$/;
exports.MassUnitRegex = /^(g|oz|lb|kg)$/;
exports.CountryUpperOrLowerRegex = /^(US|us)$/;
exports.ShippingLabelFileTypeRegex = /^(PNG|PDF|PDF_4x6|ZPLII)$/;
exports.EmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
exports.PriceRegex = /^\d+\.\d*$/;
exports.PhoneRegex2 = /\+1\d{10}/;
exports.PhoneRegex = /^[+]{0,1}[\s\./0-9]*$/;
//# sourceMappingURL=regexp-list.js.map