"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.put = exports.del = exports.post = exports.patch = exports.remove = exports.get = exports.onAPIError = exports.apiInitialize = void 0;
const axios_1 = __importDefault(require("axios"));
let api;
let errorHandler = (error) => {
    console.log(error);
};
exports.apiInitialize = (url) => {
    api = axios_1.default.create({
        baseURL: url,
        timeout: 10000,
        headers: {
            Accept: 'application/json',
        },
    });
    api.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        errorHandler(error);
        return Promise.reject(error);
    });
};
exports.onAPIError = (handleError) => {
    errorHandler = handleError;
    return () => (errorHandler = null);
};
const setAuthHeader = (jwt) => __awaiter(void 0, void 0, void 0, function* () {
    return jwt ? { Authorization: `Bearer ${jwt}` } : null;
});
exports.get = (url, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.get(url, Object.assign({ headers: header }, config));
});
exports.remove = (url, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.delete(url, Object.assign({ headers: header }, config));
});
exports.patch = (url, body, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.patch(url, body, Object.assign(Object.assign({}, config), { headers: Object.assign(Object.assign({}, header), (config ? config.headers : {})) }));
});
exports.post = (url, body, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.post(url, body, Object.assign(Object.assign({}, config), { headers: Object.assign(Object.assign({}, header), (config ? config.headers : {})) }));
});
exports.del = (url, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.delete(url, Object.assign(Object.assign({}, config), { headers: Object.assign(Object.assign({}, header), (config ? config.headers : {})) }));
});
exports.put = (url, body, config, jwt = null) => __awaiter(void 0, void 0, void 0, function* () {
    const header = yield setAuthHeader(jwt || '');
    return api.put(url, body, Object.assign({ headers: header }, config));
});
//# sourceMappingURL=http.js.map