"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.query = exports.onSearchAPIError = exports.initializeSearchAPI = void 0;
const axios_1 = __importDefault(require("axios"));
let api;
let errorHandler = (error) => {
    console.log(error);
};
exports.initializeSearchAPI = (url, { username, password, APIKey, timeout = 10000, }) => {
    api = axios_1.default.create(Object.assign({ baseURL: url, timeout, headers: Object.assign({ Accept: 'application/json' }, (APIKey ? { Authorization: APIKey } : {})) }, (!APIKey
        ? {
            auth: {
                username,
                password,
            },
        }
        : {})));
    api.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        errorHandler(error);
        return Promise.reject(error);
    });
};
exports.onSearchAPIError = (handleError) => {
    errorHandler = handleError;
    return () => (errorHandler = null);
};
exports.query = (index, data, config) => __awaiter(void 0, void 0, void 0, function* () {
    return api.post(`${index}/_search`, data, config);
});
//# sourceMappingURL=search.js.map