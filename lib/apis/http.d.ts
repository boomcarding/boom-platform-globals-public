import { AxiosResponse, AxiosRequestConfig } from 'axios';
export declare const apiInitialize: (url: string) => void;
export declare const onAPIError: (handleError: Function) => () => null;
export declare const get: <T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
export declare const remove: <T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
export declare const patch: <T = any, R = AxiosResponse<T>>(url: string, body: any, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
export declare const post: <T = any, R = AxiosResponse<T>>(url: string, body: any, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
export declare const del: <T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
export declare const put: <T = any, R = AxiosResponse<T>>(url: string, body: any, config?: AxiosRequestConfig | undefined, jwt?: string | null) => Promise<R>;
