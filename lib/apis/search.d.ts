import { AxiosResponse, AxiosRequestConfig } from 'axios';
export declare const initializeSearchAPI: (url: string, { username, password, APIKey, timeout, }: {
    username: string;
    password: string;
    APIKey: string;
    timeout: number;
}) => void;
export declare const onSearchAPIError: (handleError: Function) => () => null;
export declare const query: <T = any, R = AxiosResponse<T>>(index: string, data: any, config?: AxiosRequestConfig | undefined) => Promise<R>;
