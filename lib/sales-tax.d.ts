export interface TaxCode {
    country: string;
    state: string;
    county: string;
    city: string;
}
export interface Nexus {
    country: string;
    state: string;
}
