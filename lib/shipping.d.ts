import { Booking } from '.';
import { ModelBase } from './base-record';
import { DistanceUnit, MassUnit, RateAttributes, ShippingOrderStatus, ShipmentMethod, ShippoTrackingStatus, ShippoTransactionStatus, SignatureConfirmation, CODPaymentMethod, FedExAlcoholRecipientType, InsuranceProvider, UPSReturnServiceType, ShippoLabelType, ShippoRefundStatus, ShippoTransactionState, ShippoParcelResponseState } from './enums';
import { Money } from './money';
export interface Parcel {
    shippo_id?: string;
    length: number;
    width: number;
    height: number;
    weight: number;
    distance_unit: DistanceUnit;
    mass_unit: MassUnit;
    extra?: object;
    metadata?: string;
    template?: string;
}
export interface Rate {
    shippo_id: string;
    attributes: RateAttributes[];
    amount: Money;
    provider: string;
    service: string;
    service_token: string;
    estimated_days: number;
    duration_terms: string;
}
export interface ShippoTransaction {
    status: ShippoTransactionStatus;
    createdAt: number;
    updatedAt: number;
    shippo_id: string;
    rate: string;
    metadata?: string;
    tracking_number?: string;
    tracking_status?: ShippoTrackingStatus;
    tracking_url_provider?: string;
    eta?: number;
    label_url?: string;
}
export interface FreeShippingThreshold {
    amountSpent: Money;
    freeService: ShipmentMethod;
}
export interface ShippingPolicy extends ModelBase {
    name: string;
    merchantId: string;
    flatRate?: Money;
    itemsPerFlatRate?: number;
    freeShippingThresholds: FreeShippingThreshold[];
    pickUpOnly?: boolean;
    pickUpLocations?: string[];
}
export interface PackageDetails {
    weight: number;
    massUnit: MassUnit;
    boxId: string;
    itemsPerBox: number;
    shipsFrom: string;
}
export interface ShippingBox extends ModelBase {
    merchantId: string;
    name?: string;
    unit: DistanceUnit;
    length: number;
    width: number;
    height: number;
}
export interface ShippingOrder extends ModelBase {
    shippo_id?: string;
    trackingNumber?: string;
    trackingLink?: string;
    price: Money;
    purchaser: string;
    status: ShippingOrderStatus;
}
export interface OrderGroup {
    store: string;
    bookings: Booking[];
    shippable: boolean;
    rates: Rate[];
    selectedRate?: Rate;
}
export interface ShippingCheckoutRequest {
    bookings: Booking[];
    shipToAddressId: string;
}
export interface ShipmentExtras {
    signature_confirmation?: SignatureConfirmation;
    authority_to_leave?: boolean;
    saturday_delivery?: boolean;
    bypass_address_validation?: boolean;
    request_retail_rates?: boolean;
    COD?: {
        amount: string;
        currency: string;
        payment_method?: CODPaymentMethod;
    };
    alcohol?: {
        contains_alcohol: boolean;
        recipient_type?: FedExAlcoholRecipientType;
    };
    dry_ice?: {
        contains_dry_ice: boolean;
        weight: number;
    };
    insurance?: {
        amount: number;
        currency: string;
        content: string;
        provider?: InsuranceProvider;
    };
    is_return?: boolean;
    reference_1?: string;
    reference_2?: string;
    return_service_type?: UPSReturnServiceType;
}
export interface GetRatesRequest {
    shipToAddressId: string;
    shipFromAddressId: string;
    parcels: Parcel[];
    extra?: ShipmentExtras;
    returnAll?: boolean;
    shipmentMethods?: ShipmentMethod[];
}
export interface PurchaseRateRequest {
    shippoRateId: string;
    purchaserId: string;
    labelFileType?: ShippoLabelType;
}
export interface EstimateRateRequest {
    parcel: Parcel;
    shipmentMethod: ShipmentMethod;
    to: string;
    from: string;
}
export interface ShippingCheckoutRequest {
    bookings: Booking[];
    shipToAddressId: string;
}
export interface ShippoAddressResponse {
    is_complete: boolean;
    object_created: string;
    object_updated: string;
    object_id: string;
    object_owner: string;
    name?: string;
    company?: string;
    street1: string;
    street_no?: string;
    street2?: string;
    street3?: string;
    city?: string;
    zip?: string;
    state?: string;
    country?: string;
    phone?: string;
    email?: string;
    is_residential?: boolean;
    metadata?: string;
    test?: boolean;
    validation_results?: {
        is_valid?: boolean;
        messages?: string[];
    };
}
export interface ShippoRefundResponse {
    object_created: string;
    object_updated: string;
    object_id: string;
    object_owner: string;
    status: ShippoRefundStatus;
    transaction: string;
    test: boolean;
}
export interface ShippoResponseMessage {
    source?: string;
    code?: string;
    message?: string;
    text?: string;
}
export interface ShippoRateResponse {
    object_created: string;
    object_id: string;
    object_owner: string;
    attributes: RateAttributes[];
    amount_local: number;
    currency_local: string;
    amount: number;
    currency: string;
    provider: string;
    provider_image_75?: string;
    provider_image_200?: string;
    servicelevel?: {
        name: string;
        token: ShipmentMethod;
        terms: string;
    };
    estimated_days: number;
    duration_terms: string;
    carrier_account: string;
    zone?: string;
    messages?: ShippoResponseMessage[];
    test?: boolean;
}
export interface ShippoParcelExtras {
    COD?: {
        amount?: string;
        currency?: string;
        payment_method?: CODPaymentMethod;
    };
    insurance?: {
        amount?: string;
        currency?: string;
        provider?: 'FEDEX' | 'UPS';
        content?: string;
    };
}
export interface ShippoParcelResponse {
    object_state?: ShippoParcelResponseState;
    object_created: string;
    object_updated: string;
    object_id: string;
    object_owner: string;
    length: number;
    width: number;
    height: number;
    distance_unit: DistanceUnit;
    weight: number;
    mass_unit: MassUnit;
    template?: string;
    metadata?: string;
    extra?: ShippoParcelExtras;
    test?: boolean;
}
export interface ShippoShipmentResponse {
    status: ShippoTransactionStatus;
    object_created: string;
    object_updated: string;
    object_id: string;
    object_owner: string;
    address_from: ShippoAddressResponse;
    address_to: ShippoAddressResponse;
    parcels: ShippoParcelResponse[];
    shipment_date?: string;
    address_return?: ShippoAddressResponse;
    metadata?: string;
    extra?: ShipmentExtras;
    rates: ShippoRateResponse[];
    messages?: ShippoResponseMessage[];
    test?: boolean;
}
export interface ShippoTransactionResponse {
    object_state: ShippoTransactionState;
    status: ShippoTransactionStatus;
    object_created: string;
    object_updated: string;
    object_id: string;
    object_owner: string;
    rate: string;
    metadata?: string;
    label_file_type?: ShippoLabelType;
    test?: boolean;
    tracking_number?: string;
    tracking_status?: ShippoTrackingStatus;
    tracking_url_provider?: string;
    eta?: string;
    label_url?: string;
    commercial_invoice_url?: string;
    messages?: ShippoResponseMessage[];
    qr_code_url?: string;
}
