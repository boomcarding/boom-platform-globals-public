"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingTypes = void 0;
var BookingTypes;
(function (BookingTypes) {
    BookingTypes["OFFER"] = "offer";
    BookingTypes["PRODUCT"] = "product";
})(BookingTypes = exports.BookingTypes || (exports.BookingTypes = {}));
//# sourceMappingURL=booking-types.js.map