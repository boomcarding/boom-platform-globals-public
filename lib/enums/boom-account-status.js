"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BoomAccountStatus = void 0;
var BoomAccountStatus;
(function (BoomAccountStatus) {
    BoomAccountStatus["ACTIVE"] = "Active";
    BoomAccountStatus["CANCELLED"] = "Cancelled";
})(BoomAccountStatus = exports.BoomAccountStatus || (exports.BoomAccountStatus = {}));
//# sourceMappingURL=boom-account-status.js.map