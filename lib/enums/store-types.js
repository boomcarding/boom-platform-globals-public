"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoreTypes = void 0;
var StoreTypes;
(function (StoreTypes) {
    StoreTypes["ONLINE"] = "Online";
    StoreTypes["BRICK_AND_MORTAR"] = "Brick & Mortar";
    StoreTypes["BOTH"] = "Online and Brick & Mortar";
})(StoreTypes = exports.StoreTypes || (exports.StoreTypes = {}));
//# sourceMappingURL=store-types.js.map