export declare enum ReturnReason {
    NO_LONGER_NEEDED = "No longer needed",
    INACCURATE_DESCRIPTIONS = "Inaccurate description",
    DEFECTIVE_ITEM = "Defective item",
    MISTAKE_PURCHASE = "Item purchased by mistake",
    BETTER_PRICE = "Found a better price elsewhere",
    DAMAGED_PRODUCT = "Damaged product but box is fine",
    DAMAGED_PRODUCT_AND_BOX = "Damaged product and box",
    LATE_ARRIVAL = "Packaged arrived late",
    MISSING_OR_BROKEN_PARTS = "Item is missing parts or parts are broken",
    WRONG_ITEM = "Wrong item was delivered",
    EXTRA_ITEM = "Received extra items; no refund necessary",
    DID_NOT_APPROVE = "Did not approve purchase"
}
export declare enum ReturnMethod {
    SHIP = "Ship to merchant",
    DROP_OFF = "Drop off at store",
    NO_RETURN = "Item cannot be returned"
}
export declare enum TransactionTotalParts {
    NET_PRODUCT_COST = "Net product cost",
    BOOM_FEE = "Boom fee",
    TAX = "Sales tax",
    SHIPPING = "Shipping cost"
}
export declare enum ReturnCostType {
    FLAT_FEE = "Flat rate fee",
    SHIPPING = "Shipping fee"
}
export declare enum Status {
    REQUESTED = "Requested",
    APPROVED = "Approved",
    EXPIRED = "Expired",
    DENIED = "Denied",
    COMPLETE = "Complete",
    PROCESSING = "Processing",
    DISPUTED = "Disputed"
}
