"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Status = exports.ReturnCostType = exports.TransactionTotalParts = exports.ReturnMethod = exports.ReturnReason = void 0;
var ReturnReason;
(function (ReturnReason) {
    ReturnReason["NO_LONGER_NEEDED"] = "No longer needed";
    ReturnReason["INACCURATE_DESCRIPTIONS"] = "Inaccurate description";
    ReturnReason["DEFECTIVE_ITEM"] = "Defective item";
    ReturnReason["MISTAKE_PURCHASE"] = "Item purchased by mistake";
    ReturnReason["BETTER_PRICE"] = "Found a better price elsewhere";
    ReturnReason["DAMAGED_PRODUCT"] = "Damaged product but box is fine";
    ReturnReason["DAMAGED_PRODUCT_AND_BOX"] = "Damaged product and box";
    ReturnReason["LATE_ARRIVAL"] = "Packaged arrived late";
    ReturnReason["MISSING_OR_BROKEN_PARTS"] = "Item is missing parts or parts are broken";
    ReturnReason["WRONG_ITEM"] = "Wrong item was delivered";
    ReturnReason["EXTRA_ITEM"] = "Received extra items; no refund necessary";
    ReturnReason["DID_NOT_APPROVE"] = "Did not approve purchase";
})(ReturnReason = exports.ReturnReason || (exports.ReturnReason = {}));
var ReturnMethod;
(function (ReturnMethod) {
    ReturnMethod["SHIP"] = "Ship to merchant";
    ReturnMethod["DROP_OFF"] = "Drop off at store";
    ReturnMethod["NO_RETURN"] = "Item cannot be returned";
})(ReturnMethod = exports.ReturnMethod || (exports.ReturnMethod = {}));
var TransactionTotalParts;
(function (TransactionTotalParts) {
    TransactionTotalParts["NET_PRODUCT_COST"] = "Net product cost";
    TransactionTotalParts["BOOM_FEE"] = "Boom fee";
    TransactionTotalParts["TAX"] = "Sales tax";
    TransactionTotalParts["SHIPPING"] = "Shipping cost";
})(TransactionTotalParts = exports.TransactionTotalParts || (exports.TransactionTotalParts = {}));
var ReturnCostType;
(function (ReturnCostType) {
    ReturnCostType["FLAT_FEE"] = "Flat rate fee";
    ReturnCostType["SHIPPING"] = "Shipping fee";
})(ReturnCostType = exports.ReturnCostType || (exports.ReturnCostType = {}));
var Status;
(function (Status) {
    Status["REQUESTED"] = "Requested";
    Status["APPROVED"] = "Approved";
    Status["EXPIRED"] = "Expired";
    Status["DENIED"] = "Denied";
    Status["COMPLETE"] = "Complete";
    Status["PROCESSING"] = "Processing";
    Status["DISPUTED"] = "Disputed";
})(Status = exports.Status || (exports.Status = {}));
//# sourceMappingURL=returns.js.map