export declare enum BookingStatus {
    ACTIVE = "Active",
    USED = "Used",
    CANCELLED = "Cancelled"
}
