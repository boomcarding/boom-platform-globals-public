export declare enum RoleKey {
    All = "*",
    Member = "member",
    Merchant = "merchant",
    Admin = "admin",
    SuperAdmin = "superAdmin"
}
