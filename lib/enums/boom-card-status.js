"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BoomCardStatus = void 0;
var BoomCardStatus;
(function (BoomCardStatus) {
    BoomCardStatus["ACTIVE"] = "Active";
    BoomCardStatus["INACTIVE"] = "Inactive";
    BoomCardStatus["INACTIVE_ISSUED"] = "Inactive Issued";
    BoomCardStatus["BLOCKED"] = "Blocked";
})(BoomCardStatus = exports.BoomCardStatus || (exports.BoomCardStatus = {}));
//# sourceMappingURL=boom-card-status.js.map