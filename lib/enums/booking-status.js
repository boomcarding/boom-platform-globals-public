"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BookingStatus = void 0;
var BookingStatus;
(function (BookingStatus) {
    BookingStatus["ACTIVE"] = "Active";
    BookingStatus["USED"] = "Used";
    BookingStatus["CANCELLED"] = "Cancelled";
})(BookingStatus = exports.BookingStatus || (exports.BookingStatus = {}));
//# sourceMappingURL=booking-status.js.map