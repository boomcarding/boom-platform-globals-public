export declare enum TransactionType {
    FUNDING = "funding",
    PURCHASE = "purchase",
    CASHBACK = "cashback",
    TRANSFER = "transfer",
    RETURN = "return",
    MERCHANT_WITHDRAWAL = "merchant-withdrawal",
    CUSTOMER_BILLING = "customer-billing"
}
export declare enum TransactionStatus {
    PENDING = "pending",
    CANCELLED = "cancelled",
    COMPLETED = "completed",
    FAILED = "failed",
    UNPROCESSED = "unprocessed"
}
