"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionStatus = exports.TransactionType = void 0;
var TransactionType;
(function (TransactionType) {
    TransactionType["FUNDING"] = "funding";
    TransactionType["PURCHASE"] = "purchase";
    TransactionType["CASHBACK"] = "cashback";
    TransactionType["TRANSFER"] = "transfer";
    TransactionType["RETURN"] = "return";
    TransactionType["MERCHANT_WITHDRAWAL"] = "merchant-withdrawal";
    TransactionType["CUSTOMER_BILLING"] = "customer-billing";
})(TransactionType = exports.TransactionType || (exports.TransactionType = {}));
var TransactionStatus;
(function (TransactionStatus) {
    TransactionStatus["PENDING"] = "pending";
    TransactionStatus["CANCELLED"] = "cancelled";
    TransactionStatus["COMPLETED"] = "completed";
    TransactionStatus["FAILED"] = "failed";
    TransactionStatus["UNPROCESSED"] = "unprocessed";
})(TransactionStatus = exports.TransactionStatus || (exports.TransactionStatus = {}));
//# sourceMappingURL=transactions.js.map