"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductStatus = void 0;
var ProductStatus;
(function (ProductStatus) {
    ProductStatus["PENDING"] = "pending";
    ProductStatus["APPROVED"] = "approved";
    ProductStatus["DECLINED"] = "declined";
})(ProductStatus = exports.ProductStatus || (exports.ProductStatus = {}));
//# sourceMappingURL=product.js.map