export declare enum DistanceUnit {
    CENTIMETER = "cm",
    INCH = "in",
    FEET = "ft",
    MILLIMETER = "mm",
    METER = "m",
    YARD = "yd"
}
export declare enum MassUnit {
    GRAM = "g",
    OUNCE = "oz",
    POUND = "lb",
    KILO = "kg"
}
export declare enum ShipmentMethod {
    USPS_PRIORITY = "usps_priority",
    USPS_PRIORITY_EXP = "usps_priority_express",
    USPS_FIRST = "usps_first",
    USPS_SELECT = "usps_parcel_select",
    UPS_STANDARD = "ups_standard",
    UPS_GROUND = "ups_ground",
    UPS_SAVER = "ups_saver",
    UPS_3_DAY = "ups_3_day_select",
    UPS_2_DAY_AIR = "ups_second_day_air",
    UPS_2_DAY_AIR_AM = "ups_second_day_air_am",
    UPS_NEXT_DAY_AIR = "ups_next_day_air",
    UPS_NEXT_DAY_AIR_SAVER = "ups_next_day_air_saver",
    UPS_NEXT_DAY_AM = "ups_next_day_air_early_am",
    UPS_MAIL_INNOVATIONS = "ups_mail_innovations_domestic",
    UPS_SUREPOST = "ups_surepost",
    UPS_SUREPOST_PRINTED = "ups_surepost_bound_printed_matter",
    UPS_SUREPOST_LIGHTWEIGHT = "ups_surepost_lightweight",
    UPS_SUREPOST_MEDIA = "ups_surepost_media",
    UPS_EXPRESS = "ups_express",
    UPS_EXPRESS_1200 = "ups_express_1200",
    UPS_EXPRESS_PLUS = "ups_express_plus",
    UPS_EXPEDITED = "ups_expedited",
    SELF_SHIP = "self ship"
}
export declare enum ShippingOrderStatus {
    PAID = "paid",
    REFUNDED = "refunded",
    REFUND_PENDING = "refund pending"
}
export declare enum SignatureConfirmation {
    STANDARD = "STANDARD",
    ADULT = "ADULT",
    CERTIFIED = "CERTIFIED",
    INDIRECT = "INDIRECT",
    CARRIER_CONFIRMATION = "CARRIER_CONFIRMATION"
}
export declare enum CODPaymentMethod {
    SECURED_FUNDS = "SECURED_FUNDS",
    CASH = "CASH",
    ANY = "ANY"
}
