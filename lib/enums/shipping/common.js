"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CODPaymentMethod = exports.SignatureConfirmation = exports.ShippingOrderStatus = exports.ShipmentMethod = exports.MassUnit = exports.DistanceUnit = void 0;
var DistanceUnit;
(function (DistanceUnit) {
    DistanceUnit["CENTIMETER"] = "cm";
    DistanceUnit["INCH"] = "in";
    DistanceUnit["FEET"] = "ft";
    DistanceUnit["MILLIMETER"] = "mm";
    DistanceUnit["METER"] = "m";
    DistanceUnit["YARD"] = "yd";
})(DistanceUnit = exports.DistanceUnit || (exports.DistanceUnit = {}));
var MassUnit;
(function (MassUnit) {
    MassUnit["GRAM"] = "g";
    MassUnit["OUNCE"] = "oz";
    MassUnit["POUND"] = "lb";
    MassUnit["KILO"] = "kg";
})(MassUnit = exports.MassUnit || (exports.MassUnit = {}));
var ShipmentMethod;
(function (ShipmentMethod) {
    ShipmentMethod["USPS_PRIORITY"] = "usps_priority";
    ShipmentMethod["USPS_PRIORITY_EXP"] = "usps_priority_express";
    ShipmentMethod["USPS_FIRST"] = "usps_first";
    ShipmentMethod["USPS_SELECT"] = "usps_parcel_select";
    ShipmentMethod["UPS_STANDARD"] = "ups_standard";
    ShipmentMethod["UPS_GROUND"] = "ups_ground";
    ShipmentMethod["UPS_SAVER"] = "ups_saver";
    ShipmentMethod["UPS_3_DAY"] = "ups_3_day_select";
    ShipmentMethod["UPS_2_DAY_AIR"] = "ups_second_day_air";
    ShipmentMethod["UPS_2_DAY_AIR_AM"] = "ups_second_day_air_am";
    ShipmentMethod["UPS_NEXT_DAY_AIR"] = "ups_next_day_air";
    ShipmentMethod["UPS_NEXT_DAY_AIR_SAVER"] = "ups_next_day_air_saver";
    ShipmentMethod["UPS_NEXT_DAY_AM"] = "ups_next_day_air_early_am";
    ShipmentMethod["UPS_MAIL_INNOVATIONS"] = "ups_mail_innovations_domestic";
    ShipmentMethod["UPS_SUREPOST"] = "ups_surepost";
    ShipmentMethod["UPS_SUREPOST_PRINTED"] = "ups_surepost_bound_printed_matter";
    ShipmentMethod["UPS_SUREPOST_LIGHTWEIGHT"] = "ups_surepost_lightweight";
    ShipmentMethod["UPS_SUREPOST_MEDIA"] = "ups_surepost_media";
    ShipmentMethod["UPS_EXPRESS"] = "ups_express";
    ShipmentMethod["UPS_EXPRESS_1200"] = "ups_express_1200";
    ShipmentMethod["UPS_EXPRESS_PLUS"] = "ups_express_plus";
    ShipmentMethod["UPS_EXPEDITED"] = "ups_expedited";
    ShipmentMethod["SELF_SHIP"] = "self ship";
})(ShipmentMethod = exports.ShipmentMethod || (exports.ShipmentMethod = {}));
var ShippingOrderStatus;
(function (ShippingOrderStatus) {
    ShippingOrderStatus["PAID"] = "paid";
    ShippingOrderStatus["REFUNDED"] = "refunded";
    ShippingOrderStatus["REFUND_PENDING"] = "refund pending";
})(ShippingOrderStatus = exports.ShippingOrderStatus || (exports.ShippingOrderStatus = {}));
var SignatureConfirmation;
(function (SignatureConfirmation) {
    SignatureConfirmation["STANDARD"] = "STANDARD";
    SignatureConfirmation["ADULT"] = "ADULT";
    SignatureConfirmation["CERTIFIED"] = "CERTIFIED";
    SignatureConfirmation["INDIRECT"] = "INDIRECT";
    SignatureConfirmation["CARRIER_CONFIRMATION"] = "CARRIER_CONFIRMATION";
})(SignatureConfirmation = exports.SignatureConfirmation || (exports.SignatureConfirmation = {}));
var CODPaymentMethod;
(function (CODPaymentMethod) {
    CODPaymentMethod["SECURED_FUNDS"] = "SECURED_FUNDS";
    CODPaymentMethod["CASH"] = "CASH";
    CODPaymentMethod["ANY"] = "ANY";
})(CODPaymentMethod = exports.CODPaymentMethod || (exports.CODPaymentMethod = {}));
//# sourceMappingURL=common.js.map