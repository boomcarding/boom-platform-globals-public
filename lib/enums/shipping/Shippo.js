"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippoParcelResponseState = exports.InsuranceProvider = exports.ShippoRefundStatus = exports.ShippoTrackingStatus = exports.ShippoTransactionStatus = exports.ShippoTransactionState = exports.ShippoLabelType = exports.RateAttributes = void 0;
var RateAttributes;
(function (RateAttributes) {
    RateAttributes["CHEAPEST"] = "CHEAPEST";
    RateAttributes["FASTEST"] = "FASTEST";
    RateAttributes["BESTVALUE"] = "BESTVALUE";
    RateAttributes["FREE_SHIPPING"] = "FREE SHIPPING";
    RateAttributes["SELF_SHIP"] = "SELF SHIP";
})(RateAttributes = exports.RateAttributes || (exports.RateAttributes = {}));
var ShippoLabelType;
(function (ShippoLabelType) {
    ShippoLabelType["PNG"] = "PNG";
    ShippoLabelType["PDF"] = "PDF";
    ShippoLabelType["PDF_4x6"] = "PDF_4x6";
    ShippoLabelType["ZPLII"] = "ZPLII";
})(ShippoLabelType = exports.ShippoLabelType || (exports.ShippoLabelType = {}));
var ShippoTransactionState;
(function (ShippoTransactionState) {
    ShippoTransactionState["VALID"] = "VALID";
    ShippoTransactionState["INVALID"] = "INVALID";
})(ShippoTransactionState = exports.ShippoTransactionState || (exports.ShippoTransactionState = {}));
var ShippoTransactionStatus;
(function (ShippoTransactionStatus) {
    ShippoTransactionStatus["WAITING"] = "WAITING";
    ShippoTransactionStatus["QUEUED"] = "QUEUED";
    ShippoTransactionStatus["SUCCESS"] = "SUCCESS";
    ShippoTransactionStatus["ERROR"] = "ERROR";
    ShippoTransactionStatus["REFUNDED"] = "REFUNDED";
    ShippoTransactionStatus["REFUND_PENDING"] = "REFUNDPENDING";
    ShippoTransactionStatus["REFUND_REJECTED"] = "REFUNDREJECTED";
})(ShippoTransactionStatus = exports.ShippoTransactionStatus || (exports.ShippoTransactionStatus = {}));
var ShippoTrackingStatus;
(function (ShippoTrackingStatus) {
    ShippoTrackingStatus["UNKNOWN"] = "UNKNOWN";
    ShippoTrackingStatus["DELIVERED"] = "DELIVERED";
    ShippoTrackingStatus["TRANSIT"] = "TRANSIT";
    ShippoTrackingStatus["FAILURE"] = "FAILURE";
    ShippoTrackingStatus["RETURNED"] = "RETURNED";
})(ShippoTrackingStatus = exports.ShippoTrackingStatus || (exports.ShippoTrackingStatus = {}));
var ShippoRefundStatus;
(function (ShippoRefundStatus) {
    ShippoRefundStatus["QUEUED"] = "QUEUED";
    ShippoRefundStatus["PENDING"] = "PENDING";
    ShippoRefundStatus["SUCCESS"] = "SUCCESS";
    ShippoRefundStatus["ERROR"] = "ERROR";
})(ShippoRefundStatus = exports.ShippoRefundStatus || (exports.ShippoRefundStatus = {}));
var InsuranceProvider;
(function (InsuranceProvider) {
    InsuranceProvider["FEDEX"] = "FEDEX";
    InsuranceProvider["UPS"] = "UPS";
    InsuranceProvider["ONTRAC"] = "ONTRAC";
})(InsuranceProvider = exports.InsuranceProvider || (exports.InsuranceProvider = {}));
var ShippoParcelResponseState;
(function (ShippoParcelResponseState) {
    ShippoParcelResponseState["VALID"] = "VALID";
})(ShippoParcelResponseState = exports.ShippoParcelResponseState || (exports.ShippoParcelResponseState = {}));
//# sourceMappingURL=Shippo.js.map