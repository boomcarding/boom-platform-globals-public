"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UPSReturnServiceType = void 0;
var UPSReturnServiceType;
(function (UPSReturnServiceType) {
    UPSReturnServiceType["PRINT_AND_MAIL"] = "PRINT_AND_MAIL";
    UPSReturnServiceType["ATTEMPT_1"] = "ATTEMPT_1";
    UPSReturnServiceType["ATTEMPT_3"] = "ATTEMPT_3";
    UPSReturnServiceType["ELECTRONIC_LABEL"] = "ELECTRONIC_LABEL";
})(UPSReturnServiceType = exports.UPSReturnServiceType || (exports.UPSReturnServiceType = {}));
//# sourceMappingURL=UPS.js.map