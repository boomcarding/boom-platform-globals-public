"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FedExAlcoholRecipientType = void 0;
var FedExAlcoholRecipientType;
(function (FedExAlcoholRecipientType) {
    FedExAlcoholRecipientType["CONSUMER"] = "consumer";
    FedExAlcoholRecipientType["LICENSEE"] = "licensee";
})(FedExAlcoholRecipientType = exports.FedExAlcoholRecipientType || (exports.FedExAlcoholRecipientType = {}));
//# sourceMappingURL=FedEx.js.map