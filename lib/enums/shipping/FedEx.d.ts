export declare enum FedExAlcoholRecipientType {
    CONSUMER = "consumer",
    LICENSEE = "licensee"
}
