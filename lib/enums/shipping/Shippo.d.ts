export declare enum RateAttributes {
    CHEAPEST = "CHEAPEST",
    FASTEST = "FASTEST",
    BESTVALUE = "BESTVALUE",
    FREE_SHIPPING = "FREE SHIPPING",
    SELF_SHIP = "SELF SHIP"
}
export declare enum ShippoLabelType {
    PNG = "PNG",
    PDF = "PDF",
    PDF_4x6 = "PDF_4x6",
    ZPLII = "ZPLII"
}
export declare enum ShippoTransactionState {
    VALID = "VALID",
    INVALID = "INVALID"
}
export declare enum ShippoTransactionStatus {
    WAITING = "WAITING",
    QUEUED = "QUEUED",
    SUCCESS = "SUCCESS",
    ERROR = "ERROR",
    REFUNDED = "REFUNDED",
    REFUND_PENDING = "REFUNDPENDING",
    REFUND_REJECTED = "REFUNDREJECTED"
}
export declare enum ShippoTrackingStatus {
    UNKNOWN = "UNKNOWN",
    DELIVERED = "DELIVERED",
    TRANSIT = "TRANSIT",
    FAILURE = "FAILURE",
    RETURNED = "RETURNED"
}
export declare enum ShippoRefundStatus {
    QUEUED = "QUEUED",
    PENDING = "PENDING",
    SUCCESS = "SUCCESS",
    ERROR = "ERROR"
}
export declare enum InsuranceProvider {
    FEDEX = "FEDEX",
    UPS = "UPS",
    ONTRAC = "ONTRAC"
}
export declare enum ShippoParcelResponseState {
    VALID = "VALID"
}
