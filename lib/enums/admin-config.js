"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminConfigType = void 0;
var AdminConfigType;
(function (AdminConfigType) {
    AdminConfigType["DEFAULT_PROCESSING_RATE"] = "default-processing-rate";
    AdminConfigType["INVENTORY_TYPES"] = "inventory-type";
})(AdminConfigType = exports.AdminConfigType || (exports.AdminConfigType = {}));
//# sourceMappingURL=admin-config.js.map