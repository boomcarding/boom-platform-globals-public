"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleKey = void 0;
var RoleKey;
(function (RoleKey) {
    RoleKey["All"] = "*";
    RoleKey["Member"] = "member";
    RoleKey["Merchant"] = "merchant";
    RoleKey["Admin"] = "admin";
    RoleKey["SuperAdmin"] = "superAdmin";
})(RoleKey = exports.RoleKey || (exports.RoleKey = {}));
//# sourceMappingURL=role-key.js.map