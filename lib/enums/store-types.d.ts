export declare enum StoreTypes {
    ONLINE = "Online",
    BRICK_AND_MORTAR = "Brick & Mortar",
    BOTH = "Online and Brick & Mortar"
}
