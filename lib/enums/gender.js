"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Gender = void 0;
var Gender;
(function (Gender) {
    Gender["MALE"] = "Male";
    Gender["FEMALE"] = "Female";
    Gender["NONE"] = "";
})(Gender = exports.Gender || (exports.Gender = {}));
//# sourceMappingURL=gender.js.map