export declare enum BoomAccountStatus {
    ACTIVE = "Active",
    CANCELLED = "Cancelled"
}
