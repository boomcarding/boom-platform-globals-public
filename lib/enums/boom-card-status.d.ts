export declare enum BoomCardStatus {
    ACTIVE = "Active",
    INACTIVE = "Inactive",
    INACTIVE_ISSUED = "Inactive Issued",
    BLOCKED = "Blocked"
}
